<?php
/**
 * @file
 * rules_queue.features.inc
 */

/**
 * Implements hook_views_api().
 */
function rules_queue_views_api() {
  return array("api" => "3.0");
}
