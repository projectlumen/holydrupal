<?php
/**
 * @file
 * hd_footer.box.inc
 */

/**
 * Implements hook_default_box().
 */
function hd_footer_default_box() {
  $export = array();

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'copyright_box';
  $box->plugin_key = 'simple';
  $box->title = '<none>';
  $box->description = 'Copyright Box';
  $box->options = array(
    'body' => array(
      'value' => '<div class="rteright" style="">
	Copyright © 2013. [variable:hd_owner_company_name]<br />
	Creation of <a href="http://projectlumen.com" target="_blank">Project Lumen</a></div>
',
      'format' => 'rich_text',
    ),
  );
  $export['copyright_box'] = $box;

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'footer_contact_box';
  $box->plugin_key = 'simple';
  $box->title = 'Contact Us';
  $box->description = 'Footer Contact Box';
  $box->options = array(
    'body' => array(
      'value' => '<ul class="menu">
	<li>
		<a href="/contact">Contact Form</a></li>
	<li>
		<a href="[variable:hd_owner_company_twitter]">Twitter</a></li>
	<li>
		<a href="[variable:hd_owner_company_facebook]">Facebook</a></li>
</ul>
',
      'format' => 'rich_text',
    ),
    'additional_classes' => '',
  );
  $export['footer_contact_box'] = $box;

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'social_icon_box';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'Social Icon Box';
  $box->options = array(
    'body' => array(
      'value' => '<div class="rteright" style="">
	<a href="rss.xml" target="_blank"><img alt="RSS Icon" src="/sites/all/modules/holydrupal/hd_client/hd_footer/images/rss.png" style="width: 32px; height: 32px; margin-left: 5px; margin-right: 5px;" /></a><a href="[variable:hd_owner_company_twitter]" target="_blank"><img alt="Twitter Icon" src="/sites/all/modules/holydrupal/hd_client/hd_footer/images/twitter.png" style="width: 32px; height: 32px; margin-left: 5px; margin-right: 5px;" /></a><a href="[variable:hd_owner_company_facebook]" style="text-align: right;" target="_blank"><img alt="Facebook Icon" src="/sites/all/modules/holydrupal/hd_client/hd_footer/images/facebook.png" style="width: 32px; height: 32px; margin-left: 5px; margin-right: 5px;" /></a></div>
',
      'format' => 'rich_text',
    ),
    'additional_classes' => '',
  );
  $export['social_icon_box'] = $box;

  return $export;
}
