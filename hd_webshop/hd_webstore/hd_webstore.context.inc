<?php
/**
 * @file
 * hd_webstore.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function hd_webstore_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'portal_quotes';
  $context->description = '';
  $context->tag = 'HD Webstore';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'portal/quote' => 'portal/quote',
        'portal/quote/*' => 'portal/quote/*',
        'portal/quotes' => 'portal/quotes',
        'portal/quotes/*' => 'portal/quotes/*',
      ),
    ),
  );
  $context->reactions = array(
    'breadcrumb' => 'portal/quotes',
    'menu' => 'portal/quotes',
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('HD Webstore');
  $export['portal_quotes'] = $context;

  return $export;
}
