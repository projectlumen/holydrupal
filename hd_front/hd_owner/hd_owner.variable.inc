<?php
/**
 * @file
 * Variable API module. Definition for some xample variables
 */

/**
 * Implements hook_variable_info().
 */
function hd_owner_variable_info($options) {
#  Company Name
  $variables['hd_owner_company_name'] = array(
    'type' => 'string',
    'title' => t('Company Name', array(), $options),
    'default' => 'Your Company',
    'description' => t('', array(), $options),
    'required' => TRUE,
    'group' => 'hd_owner',
  );
#  Company Phone
  $variables['hd_owner_company_phone'] = array(
    'type' => 'string',
    'title' => t('Company Phone', array(), $options),
    'default' => '(555) 555-5555',
    'description' => t('', array(), $options),
    'required' => FALSE,
    'group' => 'hd_owner',
  );
#  Company Fax
  $variables['hd_owner_company_fax'] = array(
    'type' => 'string',
    'title' => t('Company Fax', array(), $options),
    'default' => '(555) 555-5555',
    'description' => t('', array(), $options),
    'required' => FALSE,
    'group' => 'hd_owner',
  );
#  Company Address
  $variables['hd_owner_company_address'] = array(
    'type' => 'text',
    'title' => t('Company Address', array(), $options),
    'default' => '1234 Main Ave
Somewhere, CA 90210',
    'description' => t('', array(), $options),
    'required' => FALSE,
    'group' => 'hd_owner',
  );
#  Company Twitter
  $variables['hd_owner_company_twitter'] = array(
    'type' => 'string',
    'title' => t('Company Twitter', array(), $options),
    'default' => 'http://twitter.com',
    'description' => t('', array(), $options),
    'required' => FALSE,
    'group' => 'hd_owner',
  );
#  Company Facebook
  $variables['hd_owner_company_facebook'] = array(
    'type' => 'string',
    'title' => t('Company Facebook', array(), $options),
    'default' => 'http://facebook.com',
    'description' => t('', array(), $options),
    'required' => FALSE,
    'group' => 'hd_owner',
  );
  return $variables;
}

/**
 * Implements hook_variable_group_info().
 */
function hd_owner_variable_group_info() {
  $groups['hd_owner'] = array(
    'title' => t('Owner'),
    'description' => t('Owner company and website variables.'),
    'access' => 'administer main-menu menu items',
    'path' => array('admin/config/system/variable/owner'),
  );
  return $groups;
}

