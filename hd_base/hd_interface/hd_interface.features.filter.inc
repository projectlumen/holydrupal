<?php
/**
 * @file
 * hd_interface.features.filter.inc
 */

/**
 * Implements hook_filter_default_formats().
 */
function hd_interface_filter_default_formats() {
  $formats = array();

  // Exported format: Rich text.
  $formats['rich_text'] = array(
    'format' => 'rich_text',
    'name' => 'Rich text',
    'cache' => '0',
    'status' => '1',
    'weight' => '0',
    'filters' => array(
      'filter_autop' => array(
        'weight' => '0',
        'status' => '1',
        'settings' => array(),
      ),
      'filter_url' => array(
        'weight' => '0',
        'status' => '1',
        'settings' => array(
          'filter_url_length' => '250',
        ),
      ),
      'filter_tokens' => array(
        'weight' => '0',
        'status' => '1',
        'settings' => array(),
      ),
    ),
  );

  return $formats;
}
