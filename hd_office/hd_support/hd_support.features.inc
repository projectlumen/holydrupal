<?php
/**
 * @file
 * hd_support.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function hd_support_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function hd_support_node_info() {
  $items = array(
    'ticket' => array(
      'name' => t('Ticket'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Ticket Number'),
      'help' => '',
    ),
  );
  return $items;
}
