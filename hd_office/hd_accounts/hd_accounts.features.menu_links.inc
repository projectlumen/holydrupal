<?php
/**
 * @file
 * hd_accounts.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function hd_accounts_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-office:office/dashboard
  $menu_links['menu-office:office/dashboard'] = array(
    'menu_name' => 'menu-office',
    'link_path' => 'office/dashboard',
    'router_path' => 'office/dashboard',
    'link_title' => 'Dashboard',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Dashboard');


  return $menu_links;
}
