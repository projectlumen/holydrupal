<?php
/**
 * @file
 * hd_contact.box.inc
 */

/**
 * Implements hook_default_box().
 */
function hd_contact_default_box() {
  $export = array();

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'contact_box';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'Contact Box';
  $box->options = array(
    'body' => array(
      'value' => '<table>
	<tbody>
		<tr>
			<td>
				&nbsp;</td>
			<td>
				[variable:hd_owner_company_name]</td>
		</tr>
		<tr>
			<td>
				&nbsp;</td>
			<td>
				[variable:site_mail]</td>
		</tr>
		<tr>
			<td>
				&nbsp;</td>
			<td>
				[variable:hd_owner_company_phone]</td>
		</tr>
		<tr>
			<td>
				&nbsp;</td>
			<td>
				[variable:hd_owner_company_fax]</td>
		</tr>
		<tr>
			<td>
				&nbsp;</td>
			<td>
				[variable:hd_owner_company_address]</td>
		</tr>
	</tbody>
</table>
',
      'format' => 'rich_text',
    ),
    'additional_classes' => '',
  );
  $export['contact_box'] = $box;

  return $export;
}
