<?php
/**
 * @file
 * hd_home.layouts.inc
 */

/**
 * Implements hook_default_panels_layout().
 */
function hd_home_default_panels_layout() {
  $export = array();

  $layout = new stdClass();
  $layout->disabled = FALSE; /* Edit this to true to make a default layout disabled initially */
  $layout->api_version = 1;
  $layout->name = 'home_panels';
  $layout->admin_title = 'Home Panels';
  $layout->admin_description = '';
  $layout->category = '';
  $layout->plugin = 'flexible';
  $layout->settings = array(
    'items' => array(
      'canvas' => array(
        'type' => 'row',
        'contains' => 'column',
        'children' => array(
          0 => 'main',
        ),
        'parent' => NULL,
      ),
      'main' => array(
        'type' => 'column',
        'width' => 100,
        'width_type' => '%',
        'children' => array(
          0 => 3,
          1 => 1,
          2 => 'main-row',
          3 => 4,
          4 => 5,
          5 => 6,
        ),
        'parent' => 'canvas',
      ),
      'main-row' => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'center',
        ),
        'parent' => 'main',
      ),
      'center' => array(
        'type' => 'region',
        'title' => 'Center',
        'width' => 100,
        'width_type' => '%',
        'parent' => 'main-row',
      ),
      1 => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'top',
          1 => 'top_middle',
          2 => 'top_right',
        ),
        'parent' => 'main',
        'class' => '',
      ),
      'top' => array(
        'type' => 'region',
        'title' => 'Top Left',
        'width' => '32.99527533398502',
        'width_type' => '%',
        'parent' => '1',
        'class' => '',
      ),
      3 => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'top_',
        ),
        'parent' => 'main',
        'class' => '',
      ),
      'top_' => array(
        'type' => 'region',
        'title' => 'Top',
        'width' => 100,
        'width_type' => '%',
        'parent' => '3',
        'class' => '',
      ),
      'top_middle' => array(
        'type' => 'region',
        'title' => 'Top Middle',
        'width' => '33.05087981314742',
        'width_type' => '%',
        'parent' => '1',
        'class' => '',
      ),
      'top_right' => array(
        'type' => 'region',
        'title' => 'Top Right',
        'width' => '33.95384485286757',
        'width_type' => '%',
        'parent' => '1',
        'class' => '',
      ),
      4 => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'middle_left',
          1 => 'middle_right',
        ),
        'parent' => 'main',
        'class' => '',
      ),
      5 => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'bottom',
        ),
        'parent' => 'main',
        'class' => '',
      ),
      'middle_left' => array(
        'type' => 'region',
        'title' => 'Middle Left',
        'width' => 50,
        'width_type' => '%',
        'parent' => '4',
        'class' => '',
      ),
      'middle_right' => array(
        'type' => 'region',
        'title' => 'Middle Right',
        'width' => 50,
        'width_type' => '%',
        'parent' => '4',
        'class' => '',
      ),
      6 => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'bottom_left',
          1 => 'bottom_middle',
          2 => 'bottom_right',
        ),
        'parent' => 'main',
        'class' => '',
      ),
      'bottom' => array(
        'type' => 'region',
        'title' => 'Bottom',
        'width' => 100,
        'width_type' => '%',
        'parent' => '5',
        'class' => '',
      ),
      'bottom_left' => array(
        'type' => 'region',
        'title' => 'Bottom Left',
        'width' => '33.300950119012725',
        'width_type' => '%',
        'parent' => '6',
        'class' => '',
      ),
      'bottom_middle' => array(
        'type' => 'region',
        'title' => 'Bottom Middle',
        'width' => '33.152421099754385',
        'width_type' => '%',
        'parent' => '6',
        'class' => '',
      ),
      'bottom_right' => array(
        'type' => 'region',
        'title' => 'Bottom Right',
        'width' => '33.54662878123289',
        'width_type' => '%',
        'parent' => '6',
        'class' => '',
      ),
    ),
  );
  $export['home_panels'] = $layout;

  return $export;
}
