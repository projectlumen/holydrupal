<?php
/**
 * @file
 * hd_accounts.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function hd_accounts_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'office_selective';
  $context->description = '';
  $context->tag = 'HD Office';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'office' => 'office',
        'office/*' => 'office/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'menu-menu-office' => array(
          'module' => 'menu',
          'delta' => 'menu-office',
          'region' => 'menu_bar',
          'weight' => '-10',
        ),
      ),
    ),
    'menu' => 'office',
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('HD Office');
  $export['office_selective'] = $context;

  return $export;
}
