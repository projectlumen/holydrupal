<?php
/**
 * @file
 * hd_contact.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function hd_contact_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'contact_page';
  $context->description = '';
  $context->tag = 'HD Client';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'contact' => 'contact',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'boxes-contact_box' => array(
          'module' => 'boxes',
          'delta' => 'contact_box',
          'region' => 'sidebar_right',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('HD Client');
  $export['contact_page'] = $context;

  return $export;
}
