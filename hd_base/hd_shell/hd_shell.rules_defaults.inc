<?php
/**
 * @file
 * hd_shell.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function hd_shell_default_rules_configuration() {
  $items = array();
  $items['rules_hd_shell'] = entity_import('rules_config', '{ "rules_hd_shell" : {
      "LABEL" : "HD Shell",
      "PLUGIN" : "rule set",
      "TAGS" : [ "HD Webshop" ],
      "REQUIRES" : [ "php", "rules" ],
      "USES VARIABLES" : { "command" : { "label" : "Command", "type" : "text" } },
      "RULES" : [
        { "RULE" : {
            "IF" : [
              { "php_eval" : { "code" : "return variable_get(\\u0022hd_shell_interface\\u0022);" } },
              { "php_eval" : { "code" : "return variable_get(\\u0022hd_shell_messages\\u0022);" } }
            ],
            "DO" : [
              { "php_eval" : { "code" : "$hdscript = $_SERVER[\\u0027DOCUMENT_ROOT\\u0027] . base_path() . drupal_get_path(\\u0027module\\u0027, \\u0027hd_shell\\u0027) . \\u0027\\/scripts\\/holydrupal.sh\\u0027;\\r\\n$execute = \\u0027\\/bin\\/bash\\u0027 . \\u0027 \\u0027 . $hdscript . \\u0027 \\u0027 . $command;\\r\\ndrupal_set_message($command, \\u0027status\\u0027);\\r\\nexec($execute, $return);\\r\\n$message = implode(\\u0022\\\\n\\u0022, $return);\\r\\ndrupal_set_message($message, \\u0027status\\u0027);" } }
            ],
            "LABEL" : "Run Holy Drupal Script with Messages"
          }
        },
        { "RULE" : {
            "IF" : [
              { "php_eval" : { "code" : "return variable_get(\\u0022hd_shell_interface\\u0022);" } },
              { "NOT php_eval" : { "code" : "return variable_get(\\u0022hd_shell_messages\\u0022);" } }
            ],
            "DO" : [
              { "php_eval" : { "code" : "$hdscript = $_SERVER[\\u0027DOCUMENT_ROOT\\u0027] . base_path() . drupal_get_path(\\u0027module\\u0027, \\u0027hd_shell\\u0027) . \\u0027\\/scripts\\/holydrupal.sh\\u0027;\\r\\n$execute = \\u0027\\/bin\\/bash\\u0027 . \\u0027 \\u0027 . $hdscript . \\u0027 \\u0027 . $command;\\r\\nexec($execute, $return);" } }
            ],
            "LABEL" : "Run Holy Drupal Script without Messages"
          }
        },
        { "RULE" : {
            "IF" : [
              { "NOT php_eval" : { "code" : "return variable_get(\\u0022hd_shell_interface\\u0022);" } },
              { "php_eval" : { "code" : "return variable_get(\\u0022hd_shell_messages\\u0022);" } }
            ],
            "DO" : [
              { "php_eval" : { "code" : "$hdscript = $_SERVER[\\u0027DOCUMENT_ROOT\\u0027] . base_path() . drupal_get_path(\\u0027module\\u0027, \\u0027hd_shell\\u0027) . \\u0027\\/scripts\\/holydrupal.sh\\u0027;\\r\\n$execute = \\u0027\\/bin\\/bash\\u0027 . \\u0027 \\u0027 . $hdscript . \\u0027 \\u0027 . $command;\\r\\ndrupal_set_message($command, \\u0027status\\u0027);\\r\\n\\/\\/exec($execute, $return);\\r\\n\\/\\/$message = implode(\\u0022\\\\n\\u0022, $return);\\r\\n\\/\\/drupal_set_message($message, \\u0027status\\u0027);" } }
            ],
            "LABEL" : "Just Messages"
          }
        }
      ],
      "PROVIDES VARIABLES" : [ "command" ]
    }
  }');
  return $items;
}
