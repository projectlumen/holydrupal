<?php
/**
 * @file
 * hd_accounts.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function hd_accounts_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-office.
  $menus['menu-office'] = array(
    'menu_name' => 'menu-office',
    'title' => 'Office',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Office');


  return $menus;
}
