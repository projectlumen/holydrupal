<?php
/**
 * @file
 * hd_webmaster.ds.inc
 */

/**
 * Implements hook_ds_layout_settings_info().
 */
function hd_webmaster_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|control|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'control';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_2col_stacked';
  $ds_layout->settings = array(
    'regions' => array(
      'header' => array(
        0 => 'group_control_comment',
        3 => 'field_control_comment',
      ),
      'right' => array(
        1 => 'rules_links_backup_server',
        2 => 'field_control_latest_backup',
        9 => 'group_control_buck',
        10 => 'rules_links_push_repos_to_bucket',
        12 => 'field_control_bucket_update',
        13 => 'group_control_backup',
      ),
      'left' => array(
        4 => 'group_control_bash',
        5 => 'rules_links_commit_bash',
        6 => 'group_control_hd',
        7 => 'field_control_bash_commit',
        8 => 'rules_links_commit_holydrupal',
        11 => 'field_control_hd_commit',
      ),
    ),
    'fields' => array(
      'group_control_comment' => 'header',
      'rules_links_backup_server' => 'right',
      'field_control_latest_backup' => 'right',
      'field_control_comment' => 'header',
      'group_control_bash' => 'left',
      'rules_links_commit_bash' => 'left',
      'group_control_hd' => 'left',
      'field_control_bash_commit' => 'left',
      'rules_links_commit_holydrupal' => 'left',
      'group_control_buck' => 'right',
      'rules_links_push_repos_to_bucket' => 'right',
      'field_control_hd_commit' => 'left',
      'field_control_bucket_update' => 'right',
      'group_control_backup' => 'right',
    ),
    'classes' => array(),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
  );
  $export['node|control|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|control|form';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'control';
  $ds_layout->view_mode = 'form';
  $ds_layout->layout = 'ds_2col_stacked';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'title',
      ),
      'hidden' => array(
        1 => 'pathauto',
        2 => 'name',
        3 => 'comment',
        4 => 'date',
        5 => 'alias',
        6 => 'status',
        7 => 'revision',
        8 => 'promote',
        9 => 'log',
        10 => 'sticky',
        14 => 'revision_information',
        15 => 'comment_settings',
        16 => 'path',
        18 => 'field_control_bucket_update',
        19 => 'field_control_bash_commit',
        20 => 'field_control_hd_commit',
        21 => 'field_control_latest_backup',
        22 => 'author',
        23 => 'options',
        24 => 'additional_settings',
        26 => '_add_existing_field',
      ),
      'footer' => array(
        11 => 'submit',
        12 => 'preview',
        13 => 'delete',
        25 => 'actions',
      ),
      'right' => array(
        17 => 'field_control_comment',
      ),
    ),
    'fields' => array(
      'title' => 'left',
      'pathauto' => 'hidden',
      'name' => 'hidden',
      'comment' => 'hidden',
      'date' => 'hidden',
      'alias' => 'hidden',
      'status' => 'hidden',
      'revision' => 'hidden',
      'promote' => 'hidden',
      'log' => 'hidden',
      'sticky' => 'hidden',
      'submit' => 'footer',
      'preview' => 'footer',
      'delete' => 'footer',
      'revision_information' => 'hidden',
      'comment_settings' => 'hidden',
      'path' => 'hidden',
      'field_control_comment' => 'right',
      'field_control_bucket_update' => 'hidden',
      'field_control_bash_commit' => 'hidden',
      'field_control_hd_commit' => 'hidden',
      'field_control_latest_backup' => 'hidden',
      'author' => 'hidden',
      'options' => 'hidden',
      'additional_settings' => 'hidden',
      'actions' => 'footer',
      '_add_existing_field' => 'hidden',
    ),
    'classes' => array(),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
  );
  $export['node|control|form'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|platform|form';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'platform';
  $ds_layout->view_mode = 'form';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'hidden' => array(
        0 => 'name',
        1 => 'date',
        2 => 'comment',
        3 => 'revision',
        4 => 'status',
        5 => 'title',
        6 => 'promote',
        7 => 'log',
        8 => 'field_platform_status',
        9 => 'sticky',
        10 => 'pathauto',
        11 => 'alias',
        14 => 'field_platform_alias',
        16 => 'revision_information',
        17 => 'path',
        18 => 'comment_settings',
        19 => 'author',
        20 => 'options',
        22 => 'additional_settings',
        24 => '_add_existing_field',
      ),
      'ds_content' => array(
        12 => 'submit',
        13 => 'preview',
        15 => 'delete',
        21 => 'field_platform_root',
        23 => 'actions',
      ),
    ),
    'fields' => array(
      'name' => 'hidden',
      'date' => 'hidden',
      'comment' => 'hidden',
      'revision' => 'hidden',
      'status' => 'hidden',
      'title' => 'hidden',
      'promote' => 'hidden',
      'log' => 'hidden',
      'field_platform_status' => 'hidden',
      'sticky' => 'hidden',
      'pathauto' => 'hidden',
      'alias' => 'hidden',
      'submit' => 'ds_content',
      'preview' => 'ds_content',
      'field_platform_alias' => 'hidden',
      'delete' => 'ds_content',
      'revision_information' => 'hidden',
      'path' => 'hidden',
      'comment_settings' => 'hidden',
      'author' => 'hidden',
      'options' => 'hidden',
      'field_platform_root' => 'ds_content',
      'additional_settings' => 'hidden',
      'actions' => 'ds_content',
      '_add_existing_field' => 'hidden',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 0,
  );
  $export['node|platform|form'] = $ds_layout;

  return $export;
}
