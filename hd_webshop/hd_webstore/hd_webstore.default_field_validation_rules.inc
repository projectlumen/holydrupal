<?php
/**
 * @file
 * hd_webstore.default_field_validation_rules.inc
 */

/**
 * Implements hook_default_field_validation_rule().
 */
function hd_webstore_default_field_validation_rule() {
  $export = array();

  $rule = new stdClass();
  $rule->disabled = FALSE; /* Edit this to true to make a default rule disabled initially */
  $rule->api_version = 2;
  $rule->rulename = 'Domain Validation';
  $rule->name = 'domain_validation';
  $rule->field_name = 'field_domain_name_prefix';
  $rule->col = 'value';
  $rule->entity_type = 'node';
  $rule->bundle = 'domain_form';
  $rule->validator = 'field_validation_regex_validator';
  $rule->settings = array(
    'data' => '^[a-z0-9-]*$',
    'bypass' => 0,
    'roles' => array(
      2 => 0,
      3 => 0,
      4 => 0,
      5 => 0,
      6 => 0,
    ),
    'errors' => 0,
  );
  $rule->error_message = 'A domain name must only contain lowercase letters, numbers and dashes. No underscores, spaces or capital letters.';
  $export['domain_validation'] = $rule;

  $rule = new stdClass();
  $rule->disabled = FALSE; /* Edit this to true to make a default rule disabled initially */
  $rule->api_version = 2;
  $rule->rulename = 'Subdomain Validation';
  $rule->name = 'subdomain_validation';
  $rule->field_name = 'field_website_subdomain';
  $rule->col = 'value';
  $rule->entity_type = 'node';
  $rule->bundle = 'website_form';
  $rule->validator = 'field_validation_regex_validator';
  $rule->settings = array(
    'data' => '^[a-z0-9-]*$',
    'bypass' => 0,
    'roles' => array(
      2 => 0,
      3 => 0,
      4 => 0,
      5 => 0,
      6 => 0,
    ),
    'errors' => 0,
  );
  $rule->error_message = 'The subdomain can only contain lowercase letters, numbers and dashes. No spaces, underscores, capital letters or other characters.';
  $export['subdomain_validation'] = $rule;

  return $export;
}
