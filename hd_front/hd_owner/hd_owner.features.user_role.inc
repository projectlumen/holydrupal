<?php
/**
 * @file
 * hd_owner.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function hd_owner_user_default_roles() {
  $roles = array();

  // Exported role: owner.
  $roles['owner'] = array(
    'name' => 'owner',
    'weight' => '3',
  );

  return $roles;
}
