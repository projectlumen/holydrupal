<?php
/**
 * @file
 * hd_webmaster.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function hd_webmaster_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_control_backup|node|control|default';
  $field_group->group_name = 'group_control_backup';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'control';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Backup',
    'weight' => '5',
    'children' => array(
      0 => 'field_control_latest_backup',
      1 => 'rules_links_backup_server',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
      ),
    ),
  );
  $export['group_control_backup|node|control|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_control_bash|node|control|default';
  $field_group->group_name = 'group_control_bash';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'control';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Bash',
    'weight' => '2',
    'children' => array(
      0 => 'field_control_bash_commit',
      1 => 'rules_links_commit_bash',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
      ),
    ),
  );
  $export['group_control_bash|node|control|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_control_buck|node|control|default';
  $field_group->group_name = 'group_control_buck';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'control';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Bit Bucket',
    'weight' => '4',
    'children' => array(
      0 => 'field_control_bucket_update',
      1 => 'rules_links_push_repos_to_bucket',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
      ),
    ),
  );
  $export['group_control_buck|node|control|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_control_comment|node|control|default';
  $field_group->group_name = 'group_control_comment';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'control';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Comment',
    'weight' => '0',
    'children' => array(
      0 => 'field_control_comment',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Comment',
      'instance_settings' => array(
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $export['group_control_comment|node|control|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_control_hd|node|control|default';
  $field_group->group_name = 'group_control_hd';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'control';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Holy Drupal',
    'weight' => '3',
    'children' => array(
      0 => 'field_control_hd_commit',
      1 => 'rules_links_commit_holydrupal',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
      ),
    ),
  );
  $export['group_control_hd|node|control|default'] = $field_group;

  return $export;
}
