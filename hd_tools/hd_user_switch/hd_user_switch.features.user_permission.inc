<?php
/**
 * @file
 * hd_user_switch.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function hd_user_switch_user_default_permissions() {
  $permissions = array();

  // Exported permission: switch users.
  $permissions['switch users'] = array(
    'name' => 'switch users',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'devel',
  );

  return $permissions;
}
