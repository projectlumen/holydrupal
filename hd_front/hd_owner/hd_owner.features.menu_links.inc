<?php
/**
 * @file
 * hd_owner.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function hd_owner_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-owner:admin/structure/feature-set
  $menu_links['menu-owner:admin/structure/feature-set'] = array(
    'menu_name' => 'menu-owner',
    'link_path' => 'admin/structure/feature-set',
    'router_path' => 'admin/structure/feature-set',
    'link_title' => 'Features',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-50',
  );
  // Exported menu link: menu-owner:admin/structure/menu/manage/main-menu
  $menu_links['menu-owner:admin/structure/menu/manage/main-menu'] = array(
    'menu_name' => 'menu-owner',
    'link_path' => 'admin/structure/menu/manage/main-menu',
    'router_path' => 'admin/structure/menu/manage/%',
    'link_title' => 'Menu',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '1',
    'expanded' => '0',
    'weight' => '-48',
  );
  // Exported menu link: menu-owner:admin/structure/menu/manage/main-menu/add
  $menu_links['menu-owner:admin/structure/menu/manage/main-menu/add'] = array(
    'menu_name' => 'menu-owner',
    'link_path' => 'admin/structure/menu/manage/main-menu/add',
    'router_path' => 'admin/structure/menu/manage/%/add',
    'link_title' => 'Add Menu Link',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-50',
    'parent_path' => 'admin/structure/menu/manage/main-menu',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Add Menu Link');
  t('Features');
  t('Menu');


  return $menu_links;
}
