<?php
/**
 * @file
 * hd_webstore.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function hd_webstore_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  list($module, $api) = func_get_args();
  if ($module == "field_validation" && $api == "default_field_validation_rules") {
    return array("version" => "2");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function hd_webstore_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function hd_webstore_node_info() {
  $items = array(
    'domain_form' => array(
      'name' => t('Domain Form'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Domain ID'),
      'help' => '',
    ),
    'estimate' => array(
      'name' => t('Estimate'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Estimate Number'),
      'help' => '',
    ),
    'project_form' => array(
      'name' => t('Project Form'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Project ID'),
      'help' => '',
    ),
    'website_form' => array(
      'name' => t('Website Form'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Website ID'),
      'help' => '',
    ),
  );
  return $items;
}
