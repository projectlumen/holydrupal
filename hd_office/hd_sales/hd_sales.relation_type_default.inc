<?php
/**
 * @file
 * hd_sales.relation_type_default.inc
 */

/**
 * Implements hook_relation_default_relation_types().
 */
function hd_sales_relation_default_relation_types() {
  $export = array();

  $relation_type = new stdClass();
  $relation_type->disabled = FALSE; /* Edit this to true to make a default relation_type disabled initially */
  $relation_type->api_version = 1;
  $relation_type->relation_type = 'branch_relation';
  $relation_type->label = 'is the primary company for';
  $relation_type->reverse_label = 'is a branch of';
  $relation_type->directional = 1;
  $relation_type->transitive = 0;
  $relation_type->r_unique = 0;
  $relation_type->min_arity = 2;
  $relation_type->max_arity = 2;
  $relation_type->source_bundles = array(
    0 => 'node:company',
  );
  $relation_type->target_bundles = array(
    0 => 'node:company',
  );
  $export['branch_relation'] = $relation_type;

  $relation_type = new stdClass();
  $relation_type->disabled = FALSE; /* Edit this to true to make a default relation_type disabled initially */
  $relation_type->api_version = 1;
  $relation_type->relation_type = 'company_contact';
  $relation_type->label = 'is a company for';
  $relation_type->reverse_label = 'is a contact for';
  $relation_type->directional = 1;
  $relation_type->transitive = 0;
  $relation_type->r_unique = 0;
  $relation_type->min_arity = 2;
  $relation_type->max_arity = 2;
  $relation_type->source_bundles = array(
    0 => 'node:company',
  );
  $relation_type->target_bundles = array(
    0 => 'node:contact',
  );
  $export['company_contact'] = $relation_type;

  return $export;
}
