<?php
/**
 * @file
 * hd_owner.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function hd_owner_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-owner.
  $menus['menu-owner'] = array(
    'menu_name' => 'menu-owner',
    'title' => 'Owner',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Owner');


  return $menus;
}
