<?php
/**
 * @file
 * hd_store.feeds_tamper_default.inc
 */

/**
 * Implements hook_feeds_tamper_default().
 */
function hd_store_feeds_tamper_default() {
  $export = array();

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'product_display_import-products-explode';
  $feeds_tamper->importer = 'product_display_import';
  $feeds_tamper->source = 'Products';
  $feeds_tamper->plugin_id = 'explode';
  $feeds_tamper->settings = array(
    'separator' => ',',
    'limit' => '',
    'real_separator' => ',',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Explode';
  $export['product_display_import-products-explode'] = $feeds_tamper;

  return $export;
}
