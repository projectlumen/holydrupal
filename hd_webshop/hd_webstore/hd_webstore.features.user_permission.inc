<?php
/**
 * @file
 * hd_webstore.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function hd_webstore_user_default_permissions() {
  $permissions = array();

  // Exported permission: create domain_form content.
  $permissions['create domain_form content'] = array(
    'name' => 'create domain_form content',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: create estimate content.
  $permissions['create estimate content'] = array(
    'name' => 'create estimate content',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: create project_form content.
  $permissions['create project_form content'] = array(
    'name' => 'create project_form content',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: create website_form content.
  $permissions['create website_form content'] = array(
    'name' => 'create website_form content',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any domain_form content.
  $permissions['delete any domain_form content'] = array(
    'name' => 'delete any domain_form content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: delete any estimate content.
  $permissions['delete any estimate content'] = array(
    'name' => 'delete any estimate content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: delete any project_form content.
  $permissions['delete any project_form content'] = array(
    'name' => 'delete any project_form content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: delete any website_form content.
  $permissions['delete any website_form content'] = array(
    'name' => 'delete any website_form content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: delete own domain_form content.
  $permissions['delete own domain_form content'] = array(
    'name' => 'delete own domain_form content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: delete own estimate content.
  $permissions['delete own estimate content'] = array(
    'name' => 'delete own estimate content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: delete own project_form content.
  $permissions['delete own project_form content'] = array(
    'name' => 'delete own project_form content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: delete own website_form content.
  $permissions['delete own website_form content'] = array(
    'name' => 'delete own website_form content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: edit any domain_form content.
  $permissions['edit any domain_form content'] = array(
    'name' => 'edit any domain_form content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: edit any estimate content.
  $permissions['edit any estimate content'] = array(
    'name' => 'edit any estimate content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: edit any project_form content.
  $permissions['edit any project_form content'] = array(
    'name' => 'edit any project_form content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: edit any website_form content.
  $permissions['edit any website_form content'] = array(
    'name' => 'edit any website_form content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: edit own domain_form content.
  $permissions['edit own domain_form content'] = array(
    'name' => 'edit own domain_form content',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own estimate content.
  $permissions['edit own estimate content'] = array(
    'name' => 'edit own estimate content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: edit own project_form content.
  $permissions['edit own project_form content'] = array(
    'name' => 'edit own project_form content',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own website_form content.
  $permissions['edit own website_form content'] = array(
    'name' => 'edit own website_form content',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: view any commerce_product entity.
  $permissions['view any commerce_product entity'] = array(
    'name' => 'view any commerce_product entity',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'commerce_product',
  );

  return $permissions;
}
