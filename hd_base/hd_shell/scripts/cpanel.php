<?php
// Stole this from CPanel Ops
/*
 * @file
 * Enables users to control their cpanel account operations from their drupal website.
 * Currently supported operations:
 *  - Create new mysql database.
 *  - Create new ftp account.
 *  - Create new email account.
 */

/**
 * Valid permissions for this module
 * @return array An array of valid permissions for the cpanel_ops module
 */
function cpanel_ops_permission() {
  $t = get_t();

  return array(
    'administer cpanel operations' => array(
        'title' => $t('administer cpanel operations'),
    	'description' => $t(''),
    	),
    'access cpanel operations' => array(
    	'title' => $t('access cpanel operations'),
    	'description' => $t(''),
    	),

    'create new mysql database' => array(
    	'title' => $t('create new mysql database'),
    	'description' => $t(''),
    	),

    'create new ftp account' => array(
    	'title' => $t('create new ftp account'),
    	'description' => $t(''),
    	),

    'create new email account' => array(
    	'title' => t('create new email account'),
    	'description' => $t(''),
    	)
    );
}

function cpanel_ops_menu() {
  $items = array();
  $items['admin/config/cpanel_settings'] = array(
      'title' => 'Cpanel Operations settings',
      'page callback' => 'drupal_get_form',
      'description' => 'Set your cpanel configuration settings',
      'page arguments' => array('cpanel_ops_admin_settings'),
      'access arguments' => array('administer cpanel operations'),
      'type' => MENU_NORMAL_ITEM,
  );
  $items['admin/config/cpanel_settings/cpanel'] = array(
      'title' => 'Cpanel Operations settings',
      'page callback' => 'drupal_get_form',
      'description' => 'Set your cpanel configuration settings',
      'page arguments' => array('cpanel_ops_admin_settings'),
      'access arguments' => array('administer cpanel operations'),
      'type' => MENU_DEFAULT_LOCAL_TASK,
  );
  $items['admin/config/cpanel_settings/ftp'] = array(
      'title' => 'FTP Account Settings',
      'page callback' => 'drupal_get_form',
      'description' => 'Set your ftp configuration settings',
      'page arguments' => array('cpanel_ops_admin_settings_ftp'),
      'access arguments' => array('administer cpanel operations'),
      'type' => MENU_LOCAL_TASK,
  );
  $items['admin/config/cpanel_settings/email'] = array(
      'title' => 'E-mail  Account Settings',
      'page callback' => 'drupal_get_form',
      'description' => 'Set your e-mail configuration settings',
      'page arguments' => array('cpanel_ops_admin_settings_email'),
      'access arguments' => array('administer cpanel operations'),
      'type' => MENU_LOCAL_TASK,
  );
  $items['admin/config/cpanel_settings/mysql'] = array(
      'title' => 'Mysql Database settings',
      'page callback' => 'drupal_get_form',
      'description' => 'Set your mysql database configuration settings',
      'page arguments' => array('cpanel_ops_admin_settings_mysql'),
      'access arguments' => array('administer cpanel operations'),
      'type' => MENU_LOCAL_TASK,
  );


  $items['cpanel_ops/ftp'] = array(
      'title' => 'Add FTP accounts',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('cpanel_ops_add_ftp_form'),
      'access callback' => 'check_cpanel_ftp_access',
      'type' => MENU_CALLBACK,
  );

  $items['cpanel_ops/email'] = array(
      'title' => 'Add Email accounts',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('cpanel_ops_add_email_form'),
      'access callback' => 'check_cpanel_email_access',
      'type' => MENU_CALLBACK,
  );

  $items['cpanel_ops/mysqldb'] = array(
      'title' => 'Add MySQL database',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('cpanel_ops_add_mysqldb_form'),
      'access callback' => 'check_cpanel_mysql_access',
      'type' => MENU_CALLBACK,
  );

  return $items;
}

function cpanel_ops_add_ftp_form() {

  cpanel_ops_add_ftp_form_validate();

  global $user;

  $form['cpanel_add_ftp_section'] = array(
      '#type' => 'fieldset',
      '#title' => t('Add a new ftp account')
  );

  $form['cpanel_add_ftp_section']['ftp_username'] = array(
      '#type' => 'textfield',
      '#title' => t('Username'),
      '#size' => 60,
      '#maxlength' => 64,
      '#description' => t('Please enter the ftp account username'),
      '#required' => TRUE,
  );

  $form['cpanel_add_ftp_section']['ftp_password'] = array(
      '#type' => 'password_confirm',
      '#description' => t('Enter your ftp account password, and enter it again to confirm it'),
      '#required' => TRUE,
  );

  $form['cpanel_add_ftp_section']['ftp_quota'] = array(
      '#type' => 'textfield',
      '#title' => t('Ftp Quota'),
      '#default_value' => '50',
      '#size' => 60,
      '#maxlength' => 64,
      '#description' => t('Please enter the ftp account quota (default 50mb)'),
      '#required' => TRUE,
  );

  $form['cpanel_add_ftp_section']['ftp_homedir'] = array(
      '#type' => 'textfield',
      '#title' => t('Ftp home directory'),
      '#default_value' => '/' . check_plain($user->name) . '/',
      '#size' => 60,
      '#maxlength' => 64,
      '#description' => t('Please enter the ftp home directory'),
      '#required' => TRUE,
  );

  $form['cpanel_add_ftp_section']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Create'),
  );

  return $form;
}

function cpanel_ops_add_email_form() {

  cpanel_ops_add_email_form_validate();

  $form['cpanel_add_email_section'] = array(
      '#type' => 'fieldset',
      '#title' => t('Add a new email account')
  );

  $form['cpanel_add_email_section']['email_username'] = array(
      '#type' => 'textfield',
      '#title' => t('Username'),
      '#size' => 60,
      '#maxlength' => 64,
      '#description' => t('Please enter the email account username'),
      '#required' => TRUE,
  );

  $form['cpanel_add_email_section']['email_password'] = array(
      '#type' => 'password_confirm',
      '#description' => t('Enter your email account password, and enter it again to confirm it'),
      '#required' => TRUE,
  );

  $form['cpanel_add_email_section']['email_quota'] = array(
      '#type' => 'textfield',
      '#title' => t('Email Quota'),
      '#default_value' => '20',
      '#size' => 60,
      '#maxlength' => 64,
      '#description' => t('Please enter the email account quota (default 20mb)'),
      '#required' => TRUE,
  );

  $form['cpanel_add_email_section']['email_domain'] = array(
      '#type' => 'textfield',
      '#title' => t('Email home directory'),
      '#default_value' => variable_get('configure_cpanel_domain_name', ''),
      '#size' => 60,
      '#maxlength' => 64,
      '#description' => t('Please enter the email domain that will be used after the @ sign (must be a domain that is included with your cpanel)'),
      '#required' => TRUE,
  );

  $form['cpanel_add_email_section']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Create'),
  );

  return $form;
}

function cpanel_ops_add_mysqldb_form() {

  cpanel_ops_add_mysqldb_form_validate();

  $form['cpanel_add_mysqldb_section'] = array(
      '#type' => 'fieldset',
      '#title' => t('Add a new MySQL database')
  );

  $form['cpanel_add_mysqldb_section']['mysqldb_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Database Name'),
      '#size' => 60,
      '#maxlength' => 64,
      '#description' => t('Please enter the database name'),
      '#required' => TRUE,
  );

  $form['cpanel_add_mysqldb_section']['mysql_username'] = array(
      '#type' => 'textfield',
      '#title' => t('MySQL username'),
      '#size' => 60,
      '#maxlength' => 64,
      '#description' => t('Please enter the MySQL username to associate to the database'),
      '#required' => TRUE,
  );

  $form['cpanel_add_mysqldb_section']['mysql_password'] = array(
      '#type' => 'password_confirm',
      '#description' => t('Enter your MySQL user password, and enter it again to confirm it'),
      '#required' => TRUE,
  );

  $form['cpanel_add_mysqldb_section']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Create'),
  );

  return $form;
}

function cpanel_ops_add_ftp_form_validate($form = array(), &$form_state = array()) {

  $cpanel_user = variable_get('configure_cpanel_username', '');
  $cpanel_password = variable_get('configure_cpanel_password', '');
  $cpanel_domain = variable_get('configure_cpanel_domain_name', '');
  $cpanel_skin = variable_get('configure_cpanel_skin', '');

  if (empty($cpanel_user) || empty($cpanel_password) || empty($cpanel_domain) || empty($cpanel_skin)) {
    form_set_error('empty_cpanel_config', 'Please configure cpanel first at ' . l('Cpanel settings', 'admin/settings/cpanel_settings'));
  }
}

function cpanel_ops_add_mysqldb_form_validate($form = array(), &$form_state = array()) {

  $cpanel_user = variable_get('configure_cpanel_username', '');
  $cpanel_password = variable_get('configure_cpanel_password', '');
  $cpanel_domain = variable_get('configure_cpanel_domain_name', '');
  $cpanel_skin = variable_get('configure_cpanel_skin', '');

  if (empty($cpanel_user) || empty($cpanel_password) || empty($cpanel_domain) || empty($cpanel_skin)) {
    form_set_error('empty_cpanel_config', 'Please configure cpanel first at ' . l('Cpanel settings', 'admin/settings/cpanel_settings'));
  }
}

function cpanel_ops_add_email_form_validate($form = array(), &$form_state = array()) {

  $cpanel_user = variable_get('configure_cpanel_username', '');
  $cpanel_password = variable_get('configure_cpanel_password', '');
  $cpanel_domain = variable_get('configure_cpanel_domain_name', '');
  $cpanel_skin = variable_get('configure_cpanel_skin', '');

  if (empty($cpanel_user) || empty($cpanel_password) || empty($cpanel_domain) || empty($cpanel_skin)) {
    form_set_error('empty_cpanel_config', 'Please configure cpanel first at ' . l('Cpanel settings', 'admin/settings/cpanel_settings'));
  }
}

function cpanel_ops_add_ftp_form_submit($form, &$form_state) {
  $cpanel_user = variable_get('configure_cpanel_username', '');
  $cpanel_password = variable_get('configure_cpanel_password', '');
  $cpanel_domain = variable_get('configure_cpanel_domain_name', '');
  $cpanel_skin = variable_get('configure_cpanel_skin', '');

  $ftp_user = check_plain($form_state['values']['ftp_username']);
  $ftp_password = check_plain($form_state['values']['ftp_password']);
  $ftp_quota = check_plain($form_state['values']['ftp_quota']);
  $ftp_homedir = check_plain($form_state['values']['ftp_homedir']);

  $url = 'https://' . $cpanel_user . ':' . $cpanel_password . '@' . $cpanel_domain . ':2083/frontend/' . $cpanel_skin . '/ftp/doaddftp.html?';
  $url .= 'login=' . $ftp_user . '&password=' . $ftp_password . '&homedir=' . $ftp_homedir . '&quota=' . $ftp_quota;

  $result = @file_get_contents($url);

  if ($result === FALSE) {
    form_set_error('account_create_error', 'ERROR: FTP Account not created. Please make sure you entered the correct information.');
  }
  else {
    drupal_set_message(t('Ftp account created successfully'));
  }
}

function cpanel_ops_add_email_form_submit($form, &$form_state) {
  $cpanel_user = variable_get('configure_cpanel_username', '');
  $cpanel_password = variable_get('configure_cpanel_password', '');
  $cpanel_domain = variable_get('configure_cpanel_domain_name', '');
  $cpanel_skin = variable_get('configure_cpanel_skin', '');

  $email_user = check_plain($form_state['values']['email_username']);
  $email_password = check_plain($form_state['values']['email_password']);
  $email_quota = check_plain($form_state['values']['email_quota']);
  $email_domain = check_plain($form_state['values']['email_domain']);

  $url = 'https://' . $cpanel_user . ':' . $cpanel_password . '@' . $cpanel_domain . ':2083/frontend/' . $cpanel_skin . '/mail/doaddpop.html?';
  $url .= 'email=' . $email_user . '&domain=' . $email_domain . '&password=' . $email_password . '&quota=' . $email_quota;

  $result = @file_get_contents($url);

  if ($result === FALSE) {
    form_set_error('email_create_error', 'ERROR: Email Account not created. Please make sure you entered the correct information.');
  }
  else {
    drupal_set_message(t('Email account created successfully'));
  }
}

function cpanel_ops_add_mysqldb_form_submit($form, &$form_state) {
  $cpanel_user = variable_get('configure_cpanel_username', '');
  $cpanel_password = variable_get('configure_cpanel_password', '');
  $cpanel_domain = variable_get('configure_cpanel_domain_name', '');
  $cpanel_skin = variable_get('configure_cpanel_skin', '');

  $mysqldb_name = check_plain($form_state['values']['mysqldb_name']);

  $url = 'http://' . $cpanel_user . ':' . $cpanel_password . '@' . $cpanel_domain . ':2082/frontend/' . $cpanel_skin . '/sql/addb.html?db=' . $mysqldb_name;

  $result = @file_get_contents($url);

  if ($result === FALSE) {
    form_set_error('db_create_error', 'ERROR: Database not created. Please make sure you entered the correct information.');
  }
  else {
    drupal_set_message(t('Database was created successfully'));
  }

  $mysql_username = check_plain($form_state['values']['mysql_username']);
  $mysql_password = check_plain($form_state['values']['mysql_password']);

  $url = 'http://' . $cpanel_user . ':' . $cpanel_password . '@' . $cpanel_domain . ':2082/frontend/' . $cpanel_skin . '/sql/adduser.html?user=' . $mysql_username . '&pass=' . $mysql_password;

  $result = @file_get_contents($url);

  if ($result === FALSE) {
    form_set_error('db_create_error', 'ERROR: User not created. Please make sure you entered the correct information.');
  }
  else {
    drupal_set_message(t('User was created successfully'));
  }

  $url = 'https://' . $cpanel_user . ':' . $cpanel_password . '@' . $cpanel_domain . ':2083/frontend/' . $cpanel_skin . '/sql/addusertodb.html?user=' . $cpanel_user . '_' . $mysql_username . '&db=' . $cpanel_user . '_' . $mysqldb_name . '&ALL=ALL';

  $result = @file_get_contents($url);

  if ($result === FALSE) {
    form_set_error('db_create_error', 'ERROR: User not added to database. Please make sure you entered the correct information.');
  }
  else {
    drupal_set_message(t('User was added to database successfully'));
  }
}

function cpanel_ops_admin_settings() {
  $form['configure_cpanel'] = array(
      '#type' => 'fieldset',
      '#title' => t('Configure Cpanel')
  );
  $form['configure_cpanel']['configure_cpanel_domain_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Site domain name'),
      '#default_value' => variable_get('configure_cpanel_domain_name', ''),
      '#size' => 60,
      '#maxlength' => 64,
      '#description' => t('Please enter your cpanel domain name without www part ex:(example.com)'),
      '#required' => TRUE,
  );
  $form['configure_cpanel']['configure_cpanel_skin'] = array(
      '#type' => 'textfield',
      '#title' => t('Site skin'),
      '#default_value' => variable_get('configure_cpanel_skin', ''),
      '#size' => 60,
      '#maxlength' => 64,
      '#description' => t('Please enter your cpanel skin name (the part underlined /frontend/__/index.html in your cpanel home url)'),
      '#required' => TRUE,
  );
  $form['configure_cpanel']['configure_cpanel_username'] = array(
      '#type' => 'textfield',
      '#title' => t('Cpanel username'),
      '#default_value' => variable_get('configure_cpanel_username', ''),
      '#size' => 60,
      '#maxlength' => 64,
      '#description' => t('Please enter your cpanel user name'),
      '#required' => TRUE,
  );
  $form['configure_cpanel']['configure_cpanel_password'] = array(
      '#type' => 'password_confirm',
      '#description' => t('Enter your cpanel account password, and enter it again to confirm it'),
      '#required' => TRUE,
  );

  return system_settings_form($form);
}

function cpanel_ops_admin_settings_ftp($form, &$form_state) {

  cpanel_ops_add_ftp_form_validate();

  global $user;

  $form['cpanel_add_ftp_section'] = array(
      '#type' => 'fieldset',
      '#title' => t('Configure FTP Account Settings')
  );
  $form['cpanel_add_ftp_section']['ftp_quota'] = array(
      '#type' => 'textfield',
      '#title' => t('Ftp Quota'),
      '#default_value' => variable_get('configure_ftp_quota', 50),
      '#size' => 60,
      '#maxlength' => 64,
      '#description' => t('Please enter the ftp account quota (default 50mb)'),
      '#required' => TRUE,
  );

  $form['cpanel_add_ftp_section']['ftp_homedir'] = array(
      '#type' => 'textfield',
      '#title' => t('Ftp home directory'),
      '#default_value' => variable_get('configure_ftp_homedir', '/' . check_plain($user->name) . '/'),
      '#size' => 60,
      '#maxlength' => 64,
      '#description' => t('Please enter the ftp home directory'),
      '#required' => TRUE,
  );

  $form['cpanel_add_ftp_section']['create_ftp_account'] = array(
      '#type' => 'checkbox',
      '#title' => t('Create FTP Account automatically'),
      '#default_value' => variable_get('rule_create_ftp_account', 0),
      '#description' => t("Automatically create ftp account after user register or creation."),
  );

  $form['cpanel_add_ftp_section']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save Configuration'),
  );

  return $form;
}

function cpanel_ops_admin_settings_ftp_submit($form, &$form_state) {
  $ftp_quota = $form_state['values']['ftp_quota'];
  $ftp_homedir = $form_state['values']['ftp_homedir'];
  $create_ftp = $form_state['values']['create_ftp_account'];

  variable_set('configure_ftp_quota', $ftp_quota);
  variable_set('configure_ftp_homedir', $ftp_homedir);
  variable_set('rule_create_ftp_account', $create_ftp);

  $num_updated = db_update('rules_config')
          ->fields(array(
              'active' => $create_ftp,
          ))
          ->condition('name', 'cpanel_ops_create_new_ftp_account_rule', '=')
          ->execute();
}

function cpanel_ops_admin_settings_email($form, &$form_state) {

  cpanel_ops_add_email_form_validate();

  $form['cpanel_add_email_section'] = array(
      '#type' => 'fieldset',
      '#title' => t('Configure E-mail Account Settings')
  );
  $form['cpanel_add_email_section']['email_quota'] = array(
      '#type' => 'textfield',
      '#title' => t('Email Account Quota'),
      '#default_value' => variable_get('configure_email_quota', '20'),
      '#size' => 60,
      '#maxlength' => 64,
      '#description' => t('Please enter the email account quota (default 20mb)'),
      '#required' => TRUE,
  );

  $form['cpanel_add_email_section']['email_domain'] = array(
      '#type' => 'textfield',
      '#title' => t('Email domain'),
      '#default_value' => variable_get('configure_cpanel_domain_name', ''),
      '#size' => 60,
      '#maxlength' => 64,
      '#description' => t('Please enter the email domain that will be used after the @ sign (must be a domain that is included with your cpanel)'),
      '#required' => TRUE,
  );

  $form['cpanel_add_email_section']['create_email_account'] = array(
      '#type' => 'checkbox',
      '#title' => t('Create E-mail Account automatically'),
      '#default_value' => variable_get('rule_create_email_account', 0),
      '#description' => t("Automatically create E-mail account after user register or creation."),
  );

  $form['cpanel_add_email_section']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save Configuration'),
  );

  return $form;
}

function cpanel_ops_admin_settings_email_submit($form, &$form_state) {
  $email_quota = $form_state['values']['email_quota'];
  $email_domain = $form_state['values']['email_domain'];
  $create_email = $form_state['values']['create_email_account'];

  variable_set('configure_email_quota', $email_quota);
  variable_set('configure_email_domain', $email_domain);
  variable_set('rule_create_email_account', $create_email);

  $num_updated = db_update('rules_config')
          ->fields(array(
              'active' => $create_email,
          ))
          ->condition('name', 'cpanel_ops_create_new_email_account_rule', '=')
          ->execute();
}

function cpanel_ops_admin_settings_mysql($form, &$form_state) {

  cpanel_ops_add_mysqldb_form_validate();

  $form['cpanel_add_mysql_section'] = array(
      '#type' => 'fieldset',
      '#title' => t('Configure Mysql Database Settings')
  );

  $form['cpanel_add_mysql_section']['create_mysql'] = array(
      '#type' => 'checkbox',
      '#title' => t('Create Mysql database automatically'),
      '#default_value' => variable_get('rule_create_mysql_database', 0),
      '#description' => t("Automatically create mysql database after user register or creation."),
  );

  $form['cpanel_add_mysql_section']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save Configuration'),
  );

  return $form;
}

function cpanel_ops_admin_settings_mysql_submit($form, &$form_state) {
  $create_mysql = $form_state['values']['create_mysql'];
  variable_set('rule_create_mysql_database', $create_mysql);

  $num_updated = db_update('rules_config')
          ->fields(array(
              'active' => $create_mysql,
          ))
          ->condition('name', 'cpanel_ops_create_new_mysql_db_rule', '=')
          ->execute();
}

function cpanel_ops_block_info() {
  $blocks['info'] = array(
      'info' => t('Cpanel admin block'),
      'cache' => DRUPAL_NO_CACHE,
  );
  return $blocks;
}

function cpanel_ops_block_view($delta = '') {

  $block_content = '';

  if (check_cpanel_ftp_access()) {
    $block_content .= l('Add new ftp account', 'cpanel_ops/ftp');
    $block_content .= '<br/>';
  }

  if (check_cpanel_email_access()) {
    $block_content .= l('Add new email account', 'cpanel_ops/email');
    $block_content .= '<br/>';
  }

  if (check_cpanel_mysql_access()) {
    $block_content .= l('Add new mysql db', 'cpanel_ops/mysqldb');
    $block_content .= '<br/>';
  }

  $block = array();
  $block['subject'] = t('Cpanel admin operations');
  $block['content'] = $block_content;

  return $block;
}

function check_cpanel_ftp_access() {
  return user_access('access cpanel operations') && user_access('create new ftp account');
}

function check_cpanel_email_access() {
  return user_access('access cpanel operations') && user_access('create new email account');
}

function check_cpanel_mysql_access() {
  return user_access('access cpanel operations') && user_access('create new mysql database');
}

function cpanel_ops_default_rules_configuration() {

  $rules = array();
  $rule = rules_reaction_rule();

  $rule->label = t('Create ftp Account');
  $rule->active = FALSE;

  $rule->event('user_insert')
          ->action('cpanel_ops_create_new_ftp_account', array(
              'account:select' => 'account',
          ));

  $rules['cpanel_ops_create_new_ftp_account_rule'] = $rule;

  $rule = rules_reaction_rule();
  $rule->label = t('Create E-mail Account');
  $rule->active = FALSE;

  $rule->event('user_insert')
          ->action('cpanel_ops_create_new_email_account', array(
              'account:select' => 'account',
          ));

  $rules['cpanel_ops_create_new_email_account_rule'] = $rule;

  $rule = rules_reaction_rule();
  $rule->label = t('Create Mysql Database Account');
  $rule->active = FALSE;

  $rule->event('user_insert')
          ->action('cpanel_ops_create_new_mysql_db', array(
              'account:select' => 'account',
          ));

  $rules['cpanel_ops_create_new_mysql_db_rule'] = $rule;

  return $rules;
}

function cpanel_ops_rules_action_info() {
  $actions = array();

  $actions['cpanel_ops_create_new_ftp_account'] = array(
      'label' => t('Create New FTP Account'),
      'parameter' => array(
          'account' => array(
              'type' => 'user',
              'label' => t('Current User'),
          ),
      ),
      'group' => t('Cpanel Operations'),
      'callbacks' => array(
          'execute' => 'cpanel_ops_rules_create_new_ftp_account',
      ),
  );

  $actions['cpanel_ops_create_new_email_account'] = array(
      'label' => t('Create New E-mail Account'),
      'parameter' => array(
          'account' => array(
              'type' => 'user',
              'label' => t('Current User'),
          ),
      ),
      'group' => t('Cpanel Operations'),
      'callbacks' => array(
          'execute' => 'cpanel_ops_rules_create_new_email_account',
      ),
  );

  $actions['cpanel_ops_create_new_mysql_db'] = array(
      'label' => t('Create New Mysql Database'),
      'parameter' => array(
          'account' => array(
              'type' => 'user',
              'label' => t('Current User'),
          ),
      ),
      'group' => t('Cpanel Operations'),
      'callbacks' => array(
          'execute' => 'cpanel_ops_rules_create_new_mysql_db',
      ),
  );

  return $actions;
}

function cpanel_ops_rules_create_new_ftp_account($current_user){

  $cpanel_user = variable_get('configure_cpanel_username', '');
  $cpanel_password = variable_get('configure_cpanel_password', '');
  $cpanel_domain = variable_get('configure_cpanel_domain_name', '');
  $cpanel_skin = variable_get('configure_cpanel_skin', '');

  $ftp_user = $current_user->name;
  $ftp_password = user_password();
  $ftp_quota = variable_get('configure_ftp_quota', 50);
  $ftp_homedir = variable_get('configure_ftp_homedir', '/' . check_plain($current_user->name) . '/');

  $url = 'https://' . $cpanel_user . ':' . $cpanel_password . '@' . $cpanel_domain . ':2083/frontend/' . $cpanel_skin . '/ftp/doaddftp.html?';
  $url .= 'login=' . $ftp_user . '&password=' . $ftp_password . '&homedir=' . $ftp_homedir . '&quota=' . $ftp_quota;

  $result = @file_get_contents($url);

  if ($result === FALSE) {
    watchdog('Account Error', 'ERROR: FTP Account not created. Please make sure you entered the correct information.');
  }
  else {
    watchdog('FTP Account', 'Ftp account created successfully');
    $current = user_load($current_user->uid);
    if(property_exists($current, 'data')){
      $current->data['ftp_password'] = $ftp_password;
    }
    else{
      $current->data = array('ftp_password' => $ftp_password);
    }
    $normal = user_save($current);
  }
}

function cpanel_ops_rules_create_new_email_account($current_user){
  $cpanel_user = variable_get('configure_cpanel_username', '');
  $cpanel_password = variable_get('configure_cpanel_password', '');
  $cpanel_domain = variable_get('configure_cpanel_domain_name', '');
  $cpanel_skin = variable_get('configure_cpanel_skin', '');

  $email_user = $current_user->name;
  $email_password = user_password();
  $email_quota = variable_get('configure_email_quota', 20);
  $email_domain = variable_get('configure_email_domain', $cpanel_domain);

  $url = 'https://' . $cpanel_user . ':' . $cpanel_password . '@' . $cpanel_domain . ':2083/frontend/' . $cpanel_skin . '/mail/doaddpop.html?';
  $url .= 'email=' . $email_user . '&domain=' . $email_domain . '&password=' . $email_password . '&quota=' . $email_quota;

  $result = @file_get_contents($url);

  if ($result === FALSE) {
    watchdog('E-mail Error', 'ERROR: Email Account not created. Please make sure you entered the correct information.');
  }
  else {
    watchdog('E-mail Account', 'Email account created successfully');
    $current = user_load($current_user->uid);
    if(property_exists($current, 'data')){
      $current->data['email_password'] = $email_password;
    }
    else{
      $current->data = array('email_password' => $email_password);
    }
    $normal = user_save($current);
  }
}

function cpanel_ops_rules_create_new_mysql_db($current_user){

  $cpanel_user = variable_get('configure_cpanel_username', '');
  $cpanel_password = variable_get('configure_cpanel_password', '');
  $cpanel_domain = variable_get('configure_cpanel_domain_name', '');
  $cpanel_skin = variable_get('configure_cpanel_skin', '');

  $mysqldb_name = $current_user->name;

  $url = 'http://' . $cpanel_user . ':' . $cpanel_password . '@' . $cpanel_domain . ':2082/frontend/' . $cpanel_skin . '/sql/addb.html?db=' . $mysqldb_name;

  $result = @file_get_contents($url);

  if ($result === FALSE) {
    watchdog('Database', 'ERROR: Database not created. Please make sure you entered the correct information.');
  }
  else {
    watchdog('Database', 'Database was created successfully');
  }

  $mysql_username = $current_user->name;
  $mysql_password = user_password();

  $url = 'http://' . $cpanel_user . ':' . $cpanel_password . '@' . $cpanel_domain . ':2082/frontend/' . $cpanel_skin . '/sql/adduser.html?user=' . $mysql_username . '&pass=' . $mysql_password;

  $result = @file_get_contents($url);

  if ($result === FALSE) {
    watchdog('Database User', 'ERROR: User not created. Please make sure you entered the correct information.');
  }
  else {
    watchdog('Database User','User was created successfully');
  }

  $url = 'https://' . $cpanel_user . ':' . $cpanel_password . '@' . $cpanel_domain . ':2083/frontend/' . $cpanel_skin . '/sql/addusertodb.html?user=' . $cpanel_user . '_' . $mysql_username . '&db=' . $cpanel_user . '_' . $mysqldb_name . '&ALL=ALL';

  $result = @file_get_contents($url);

  if ($result === FALSE) {
    watchdog('Database User Integration', 'ERROR: User not added to database. Please make sure you entered the correct information.');
  }
  else {
    watchdog('Database User Integration', 'User was added to database successfully');
    $current = user_load($current_user->uid);
    if(property_exists($current, 'data')){
      $current->data['mysql_password'] = $mysql_password;
    }
    else{
      $current->data = array('mysql_password' => $mysql_password);
    }
    $normal = user_save($current);
  }
}