<?php
/**
 * @file
 * hd_webmaster.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function hd_webmaster_user_default_permissions() {
  $permissions = array();

  // Exported permission: create control content.
  $permissions['create control content'] = array(
    'name' => 'create control content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: create domain content.
  $permissions['create domain content'] = array(
    'name' => 'create domain content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: create email_address content.
  $permissions['create email_address content'] = array(
    'name' => 'create email_address content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: create field_site_domain_option.
  $permissions['create field_site_domain_option'] = array(
    'name' => 'create field_site_domain_option',
    'roles' => array(
      'administrator' => 'administrator',
      'manager' => 'manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: create platform content.
  $permissions['create platform content'] = array(
    'name' => 'create platform content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: create site content.
  $permissions['create site content'] = array(
    'name' => 'create site content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: create site_theme content.
  $permissions['create site_theme content'] = array(
    'name' => 'create site_theme content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: create template content.
  $permissions['create template content'] = array(
    'name' => 'create template content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: delete any control content.
  $permissions['delete any control content'] = array(
    'name' => 'delete any control content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: delete any domain content.
  $permissions['delete any domain content'] = array(
    'name' => 'delete any domain content',
    'roles' => array(
      'administrator' => 'administrator',
      'manager' => 'manager',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any email_address content.
  $permissions['delete any email_address content'] = array(
    'name' => 'delete any email_address content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: delete any platform content.
  $permissions['delete any platform content'] = array(
    'name' => 'delete any platform content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: delete any site content.
  $permissions['delete any site content'] = array(
    'name' => 'delete any site content',
    'roles' => array(
      'administrator' => 'administrator',
      'manager' => 'manager',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any site_theme content.
  $permissions['delete any site_theme content'] = array(
    'name' => 'delete any site_theme content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: delete any template content.
  $permissions['delete any template content'] = array(
    'name' => 'delete any template content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: delete own control content.
  $permissions['delete own control content'] = array(
    'name' => 'delete own control content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: delete own domain content.
  $permissions['delete own domain content'] = array(
    'name' => 'delete own domain content',
    'roles' => array(
      'administrator' => 'administrator',
      'manager' => 'manager',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own email_address content.
  $permissions['delete own email_address content'] = array(
    'name' => 'delete own email_address content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: delete own platform content.
  $permissions['delete own platform content'] = array(
    'name' => 'delete own platform content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: delete own site content.
  $permissions['delete own site content'] = array(
    'name' => 'delete own site content',
    'roles' => array(
      'administrator' => 'administrator',
      'manager' => 'manager',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own site_theme content.
  $permissions['delete own site_theme content'] = array(
    'name' => 'delete own site_theme content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: delete own template content.
  $permissions['delete own template content'] = array(
    'name' => 'delete own template content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: edit any control content.
  $permissions['edit any control content'] = array(
    'name' => 'edit any control content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: edit any domain content.
  $permissions['edit any domain content'] = array(
    'name' => 'edit any domain content',
    'roles' => array(
      'administrator' => 'administrator',
      'manager' => 'manager',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any email_address content.
  $permissions['edit any email_address content'] = array(
    'name' => 'edit any email_address content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: edit any platform content.
  $permissions['edit any platform content'] = array(
    'name' => 'edit any platform content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: edit any site content.
  $permissions['edit any site content'] = array(
    'name' => 'edit any site content',
    'roles' => array(
      'administrator' => 'administrator',
      'manager' => 'manager',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any site_theme content.
  $permissions['edit any site_theme content'] = array(
    'name' => 'edit any site_theme content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: edit any template content.
  $permissions['edit any template content'] = array(
    'name' => 'edit any template content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: edit field_site_domain_option.
  $permissions['edit field_site_domain_option'] = array(
    'name' => 'edit field_site_domain_option',
    'roles' => array(
      'administrator' => 'administrator',
      'manager' => 'manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own control content.
  $permissions['edit own control content'] = array(
    'name' => 'edit own control content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: edit own domain content.
  $permissions['edit own domain content'] = array(
    'name' => 'edit own domain content',
    'roles' => array(
      'administrator' => 'administrator',
      'manager' => 'manager',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own email_address content.
  $permissions['edit own email_address content'] = array(
    'name' => 'edit own email_address content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: edit own field_site_domain_option.
  $permissions['edit own field_site_domain_option'] = array(
    'name' => 'edit own field_site_domain_option',
    'roles' => array(
      'administrator' => 'administrator',
      'manager' => 'manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own platform content.
  $permissions['edit own platform content'] = array(
    'name' => 'edit own platform content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: edit own site content.
  $permissions['edit own site content'] = array(
    'name' => 'edit own site content',
    'roles' => array(
      'administrator' => 'administrator',
      'manager' => 'manager',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own site_theme content.
  $permissions['edit own site_theme content'] = array(
    'name' => 'edit own site_theme content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: edit own template content.
  $permissions['edit own template content'] = array(
    'name' => 'edit own template content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: view field_site_domain_option.
  $permissions['view field_site_domain_option'] = array(
    'name' => 'view field_site_domain_option',
    'roles' => array(
      'administrator' => 'administrator',
      'manager' => 'manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view own field_site_domain_option.
  $permissions['view own field_site_domain_option'] = array(
    'name' => 'view own field_site_domain_option',
    'roles' => array(
      'administrator' => 'administrator',
      'manager' => 'manager',
    ),
    'module' => 'field_permissions',
  );

  return $permissions;
}
