<?php
/**
 * @file
 * rules_queue.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function rules_queue_default_rules_configuration() {
  $items = array();
  $items['rules_create_and_populate_a_queue'] = entity_import('rules_config', '{ "rules_create_and_populate_a_queue" : {
      "LABEL" : "Create and Populate a Queue",
      "PLUGIN" : "action set",
      "TAGS" : [ "Rules Queue" ],
      "REQUIRES" : [ "php", "rules" ],
      "USES VARIABLES" : {
        "queue_name" : { "label" : "Queue Name", "type" : "text" },
        "queue_item" : { "label" : "Queue Item", "type" : "text" }
      },
      "ACTION SET" : [
        { "php_eval" : { "code" : "$queue = DrupalQueue::get($queue_name);\\r\\n$queue-\\u003EcreateItem($queue_item);\\r\\n" } }
      ]
    }
  }');
  $items['rules_delete_queue_item'] = entity_import('rules_config', '{ "rules_delete_queue_item" : {
      "LABEL" : "Delete Queue Item",
      "PLUGIN" : "action set",
      "TAGS" : [ "Rules Queue" ],
      "REQUIRES" : [ "php", "rules" ],
      "USES VARIABLES" : { "queue_name" : { "label" : "Queue Name", "type" : "text" } },
      "ACTION SET" : [
        { "php_eval" : { "code" : "$queue = DrupalQueue::get($queue_name);\\r\\n$queue_item = $queue-\\u003EclaimItem();\\r\\n$queue-\\u003EdeleteItem($queue_item);\\r\\n" } }
      ]
    }
  }');
  $items['rules_get_queue_items'] = entity_import('rules_config', '{ "rules_get_queue_items" : {
      "LABEL" : "Get Queue Items",
      "PLUGIN" : "action set",
      "TAGS" : [ "Rules Queue" ],
      "REQUIRES" : [ "php", "rules" ],
      "USES VARIABLES" : {
        "queue_name" : { "label" : "Queue Name", "type" : "text" },
        "queue_items" : {
          "label" : "Queue Items",
          "type" : "list\\u003Cqueue\\u003E",
          "parameter" : false
        }
      },
      "ACTION SET" : [
        { "php_eval" : { "code" : "$queue = DrupalQueue::get($queue_name);\\r\\n$queue_items = $queue-\\u003EclaimItem()" } }
      ],
      "PROVIDES VARIABLES" : [ "queue_items" ]
    }
  }');
  $items['rules_process_items_of_a_queue'] = entity_import('rules_config', '{ "rules_process_items_of_a_queue" : {
      "LABEL" : "Process Items of a Queue",
      "PLUGIN" : "action set",
      "TAGS" : [ "Rules Queue" ],
      "REQUIRES" : [ "rules", "php" ],
      "USES VARIABLES" : { "queue_name" : { "label" : "Queue Name", "type" : "text" } },
      "ACTION SET" : [
        { "component_rules_get_queue_items" : {
            "USING" : { "queue_name" : "tester" },
            "PROVIDE" : { "queue_items" : { "queue_items" : "Queue Items" } }
          }
        },
        { "drupal_message" : { "message" : "\\u003C?php return $queue_items; ?\\u003E" } },
        { "LOOP" : {
            "USING" : { "list" : [ "queue-items" ] },
            "ITEM" : { "list_item" : "Current list item" },
            "DO" : []
          }
        }
      ]
    }
  }');
  return $items;
}
