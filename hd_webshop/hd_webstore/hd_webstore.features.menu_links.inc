<?php
/**
 * @file
 * hd_webstore.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function hd_webstore_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu:node/add/domain-form
  $menu_links['main-menu:node/add/domain-form'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/add/domain-form',
    'router_path' => 'node/add',
    'link_title' => 'Get a Domain Name',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -10,
  );
  // Exported menu link: main-menu:node/add/project-form
  $menu_links['main-menu:node/add/project-form'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/add/project-form',
    'router_path' => 'node/add',
    'link_title' => 'Start a Project',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -30,
  );
  // Exported menu link: main-menu:node/add/website-form
  $menu_links['main-menu:node/add/website-form'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/add/website-form',
    'router_path' => 'node/add/website-form',
    'link_title' => 'Build a Website',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -20,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Build a Website');
  t('Get a Domain Name');
  t('Start a Project');


  return $menu_links;
}
