<?php
/**
 * @file
 * hd_home.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function hd_home_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'home_page';
  $context->description = '';
  $context->tag = 'HD Client';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'region' => array(
      'artisteer' => array(
        'disable' => array(
          'sidebar_left' => 'sidebar_left',
          'vnavigation_left' => 'vnavigation_left',
          'sidebar_right' => 'sidebar_right',
          'vnavigation_right' => 'vnavigation_right',
          'art_header' => 'art_header',
          'banner1' => 'banner1',
          'banner2' => 'banner2',
          'banner3' => 'banner3',
          'banner4' => 'banner4',
          'banner5' => 'banner5',
          'banner6' => 'banner6',
          'user1' => 'user1',
          'user2' => 'user2',
          'user3' => 'user3',
          'user4' => 'user4',
          'extra1' => 'extra1',
          'extra2' => 'extra2',
          'top1' => 'top1',
          'top2' => 'top2',
          'top3' => 'top3',
          'bottom1' => 'bottom1',
          'bottom2' => 'bottom2',
          'bottom3' => 'bottom3',
          'content' => 0,
          'navigation' => 0,
          'copyright' => 0,
          'footer_message' => 0,
          'dashboard_main' => 0,
          'dashboard_sidebar' => 0,
          'dashboard_inactive' => 0,
        ),
      ),
      'bartik' => array(
        'disable' => array(
          'header' => 0,
          'help' => 0,
          'page_top' => 0,
          'page_bottom' => 0,
          'highlighted' => 0,
          'featured' => 0,
          'content' => 0,
          'sidebar_first' => 0,
          'sidebar_second' => 0,
          'triptych_first' => 0,
          'triptych_middle' => 0,
          'triptych_last' => 0,
          'footer_firstcolumn' => 0,
          'footer_secondcolumn' => 0,
          'footer_thirdcolumn' => 0,
          'footer_fourthcolumn' => 0,
          'footer' => 0,
          'dashboard_main' => 0,
          'dashboard_sidebar' => 0,
          'dashboard_inactive' => 0,
        ),
      ),
      'rubik' => array(
        'disable' => array(
          'content' => 0,
          'help' => 0,
          'page_top' => 0,
          'page_bottom' => 0,
          'dashboard_main' => 0,
          'dashboard_sidebar' => 0,
          'dashboard_inactive' => 0,
        ),
      ),
      'seven' => array(
        'disable' => array(
          'content' => 0,
          'help' => 0,
          'page_top' => 0,
          'page_bottom' => 0,
          'sidebar_first' => 0,
          'dashboard_main' => 0,
          'dashboard_sidebar' => 0,
          'dashboard_inactive' => 0,
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('HD Client');
  $export['home_page'] = $context;

  return $export;
}
