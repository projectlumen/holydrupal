<?php
/**
 * @file
 * hd_store.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function hd_store_default_rules_configuration() {
  $items = array();
  $items['rules_adjust_paypal_address'] = entity_import('rules_config', '{ "rules_adjust_paypal_address" : {
      "LABEL" : "Adjust Paypal Address",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "HD Commerce" ],
      "REQUIRES" : [ "php", "rules" ],
      "ON" : [ "cron" ],
      "DO" : [
        { "component_rules_hd_shell" : {
            "USING" : { "command" : "\\u003C?php\\r\\n$ppemail = variable_get(\\u0027hd_store_paypal\\u0027, \\u0027projectlumen@gmail.com\\u0027);\\r\\n$cmd = \\u0022ppemail\\u0022;\\r\\n$site_dir = \\u0022\\/home\\/aaron\\/public_html\\/dev\\/sites\\/store.arkoin.org\\u0022;\\r\\n$execute = $cmd . \\u0027 \\u0027 . $ppemail . \\u0027 \\u0027 . $site_dir;\\r\\nreturn $execute;\\r\\n?\\u003E" },
            "PROVIDE" : { "command" : { "command" : "Command" } }
          }
        }
      ]
    }
  }');
  $items['rules_hd_store_paypal'] = entity_import('rules_config', '{ "rules_hd_store_paypal" : {
      "LABEL" : "HD Store Paypal",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "HD Store" ],
      "REQUIRES" : [ "commerce_payment" ],
      "ON" : [ "commerce_payment_methods" ],
      "DO" : [
        { "commerce_payment_enable_paypal_wps" : {
            "commerce_order" : [ "commerce-order" ],
            "payment_method" : { "value" : {
                "method_id" : "paypal_wps",
                "settings" : {
                  "business" : "aaron@arkoin.org",
                  "currency_code" : "USD",
                  "allow_supported_currencies" : 0,
                  "language" : "US",
                  "server" : "sandbox",
                  "payment_action" : "sale",
                  "ipn_logging" : "notification",
                  "ipn_create_billing_profile" : 1,
                  "show_payment_instructions" : 0
                }
              }
            }
          }
        }
      ]
    }
  }');
  return $items;
}
