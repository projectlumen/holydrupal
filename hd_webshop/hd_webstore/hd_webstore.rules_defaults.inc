<?php
/**
 * @file
 * hd_webstore.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function hd_webstore_default_rules_configuration() {
  $items = array();
  $items['rules_checking_out'] = entity_import('rules_config', '{ "rules_checking_out" : {
      "LABEL" : "Checking Out",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "HD Webstore" ],
      "REQUIRES" : [ "rules", "pathrules", "commerce_rules_extra" ],
      "ON" : [ "process_checkout_pane" ],
      "IF" : [
        { "data_is" : { "data" : [ "site:current-cart-order:state" ], "value" : "checkout" } },
        { "pathrules_checkpath" : { "path" : "[^=]checkout\\/[0-9]+\\/checkout", "operation" : "regex" } }
      ],
      "DO" : [
        { "LOOP" : {
            "USING" : { "list" : [ "site:current-cart-order:commerce-line-items" ] },
            "ITEM" : { "line_item" : "Line Item" },
            "DO" : [
              { "component_rules_ownership_recursion" : { "line_item" : [ "line_item" ], "user" : [ "site:current-user" ] } }
            ]
          }
        },
        { "component_rules_process_checkout" : {
            "order" : [ "site:current-cart-order" ],
            "user" : [ "site:current-user" ]
          }
        }
      ]
    }
  }');
  $items['rules_completing_checkout'] = entity_import('rules_config', '{ "rules_completing_checkout" : {
      "LABEL" : "Completing Checkout",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "HD Webstore" ],
      "REQUIRES" : [ "rules", "commerce_checkout" ],
      "ON" : [ "commerce_checkout_complete" ],
      "IF" : [
        { "entity_has_field" : { "entity" : [ "commerce-order" ], "field" : "field_next_order" } },
        { "NOT data_is_empty" : { "data" : [ "commerce-order:field-next-order" ] } }
      ],
      "DO" : [
        { "data_set" : {
            "data" : [ "commerce-order:field-next-order:status" ],
            "value" : "cart"
          }
        }
      ]
    }
  }');
  $items['rules_display_price_default'] = entity_import('rules_config', '{ "rules_display_price_default" : {
      "LABEL" : "Display Price Default",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "HD Webstore" ],
      "REQUIRES" : [ "rules", "entity" ],
      "ON" : [ "commerce_product_presave" ],
      "IF" : [
        { "entity_has_field" : { "entity" : [ "commerce-product" ], "field" : "field_display_price" } },
        { "data_is_empty" : { "data" : [ "commerce-product:field-display-price" ] } }
      ],
      "DO" : [
        { "data_set" : {
            "data" : [ "commerce-product:field-display-price" ],
            "value" : "$[commerce-product:commerce-price:amount-decimal]"
          }
        }
      ]
    }
  }');
  $items['rules_domain_form_create'] = entity_import('rules_config', '{ "rules_domain_form_create" : {
      "LABEL" : "Domain Form Create",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "HD Webstore" ],
      "REQUIRES" : [ "rules", "commerce_cart" ],
      "ON" : [ "node_insert" ],
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "domain_form" : "domain_form" } }
          }
        }
      ],
      "DO" : [
        { "commerce_cart_product_add_by_sku" : {
            "USING" : {
              "user" : [ "site:current-user" ],
              "sku" : "DOMAIN",
              "quantity" : "1",
              "combine" : 0
            },
            "PROVIDE" : { "product_add_line_item" : { "domain_in_cart" : "Domain in cart" } }
          }
        },
        { "component_rules_reference_line_item_to_node" : { "line_item" : [ "domain-in-cart" ], "node" : [ "node" ] } },
        { "component_rules_set_line_item_labels" : { "line_item" : [ "domain-in-cart" ], "node" : [ "node" ] } },
        { "redirect" : { "url" : "cart" } }
      ]
    }
  }');
  $items['rules_domain_form_update'] = entity_import('rules_config', '{ "rules_domain_form_update" : {
      "LABEL" : "Domain Form Update",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "HD Webstore" ],
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_update" ],
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "domain_form" : "domain_form" } }
          }
        },
        { "entity_has_field" : { "entity" : [ "node" ], "field" : "field_line_item_reference" } }
      ],
      "DO" : [
        { "component_rules_set_line_item_labels" : { "line_item" : [ "node:field-line-item-reference" ] } }
      ]
    }
  }');
  $items['rules_estimate_only'] = entity_import('rules_config', '{ "rules_estimate_only" : {
      "LABEL" : "Estimate Only",
      "PLUGIN" : "rule",
      "TAGS" : [ "HD Webstore" ],
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "line_item" : { "label" : "Line Item", "type" : "commerce_line_item" } },
      "IF" : [
        { "entity_has_field" : { "entity" : [ "line-item" ], "field" : "commerce_product" } },
        { "data_is" : { "data" : [ "line-item:commerce-product:sku" ], "value" : "ESTIMATE" } },
        { "entity_has_field" : { "entity" : [ "line-item" ], "field" : "field_node_reference" } },
        { "entity_has_field" : {
            "entity" : [ "line-item:field-node-reference" ],
            "field" : "field_node_reference"
          }
        },
        { "entity_has_field" : {
            "entity" : [ "line-item:field-node-reference:field-node-reference" ],
            "field" : "field_quote_order"
          }
        }
      ],
      "DO" : [
        { "list_remove" : {
            "list" : [ "line-item:order:commerce-line-items" ],
            "item" : [ "line-item" ]
          }
        },
        { "data_set" : {
            "data" : [
              "line-item:field-node-reference:field-node-reference:field-quote-order:status"
            ],
            "value" : "request_for_quotation"
          }
        },
        { "redirect" : { "url" : "portal\\/quote\\/[line-item:field-node-reference:field-node-reference:field-quote-order:order-number]" } },
        { "drupal_message" : { "message" : "Thank you for your project request, we have received it and will reply with an estimate shortly." } }
      ]
    }
  }');
  $items['rules_estimate_plus'] = entity_import('rules_config', '{ "rules_estimate_plus" : {
      "LABEL" : "Estimate Plus",
      "PLUGIN" : "rule",
      "TAGS" : [ "HD Webstore" ],
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : {
        "line_item" : { "label" : "Line Item", "type" : "commerce_line_item" },
        "product_order" : { "label" : "Product Order", "type" : "commerce_order" }
      },
      "IF" : [
        { "entity_has_field" : { "entity" : [ "line-item" ], "field" : "commerce_product" } },
        { "data_is" : { "data" : [ "line-item:commerce-product:sku" ], "value" : "ESTIMATE" } },
        { "entity_has_field" : { "entity" : [ "line-item" ], "field" : "field_node_reference" } },
        { "entity_has_field" : {
            "entity" : [ "line-item:field-node-reference" ],
            "field" : "field_node_reference"
          }
        },
        { "entity_has_field" : {
            "entity" : [ "line-item:field-node-reference:field-node-reference" ],
            "field" : "field_quote_order"
          }
        },
        { "entity_has_field" : { "entity" : [ "product-order" ], "field" : "field_next_order" } }
      ],
      "DO" : [
        { "data_set" : {
            "data" : [
              "line-item:field-node-reference:field-node-reference:field-quote-order:status"
            ],
            "value" : "request_for_quotation"
          }
        },
        { "drupal_message" : { "message" : "Thank you for your project request, we have received it and will reply with an estimate shortly." } },
        { "list_remove" : {
            "list" : [ "line-item:order:commerce-line-items" ],
            "item" : [ "line-item" ]
          }
        }
      ]
    }
  }');
  $items['rules_ownership_recursion'] = entity_import('rules_config', '{ "rules_ownership_recursion" : {
      "LABEL" : "Ownership Recursion",
      "PLUGIN" : "rule set",
      "TAGS" : [ "HD Webstore" ],
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : {
        "line_item" : { "label" : "Line Item", "type" : "commerce_line_item" },
        "user" : { "label" : "User", "type" : "user" }
      },
      "RULES" : [
        { "RULE" : {
            "IF" : [
              { "entity_has_field" : { "entity" : [ "line-item" ], "field" : "commerce_product" } },
              { "data_is" : { "data" : [ "line-item:commerce-product:sku" ], "value" : "ESTIMATE" } },
              { "entity_has_field" : { "entity" : [ "line-item" ], "field" : "field_node_reference" } },
              { "entity_has_field" : {
                  "entity" : [ "line-item:field-node-reference" ],
                  "field" : "field_node_reference"
                }
              },
              { "entity_has_field" : {
                  "entity" : [ "line-item:field-node-reference:field-node-reference" ],
                  "field" : "field_quote_order"
                }
              }
            ],
            "DO" : [
              { "data_set" : {
                  "data" : [ "line-item:field-node-reference:author" ],
                  "value" : [ "user" ]
                }
              },
              { "data_set" : {
                  "data" : [ "line-item:field-node-reference:field-node-reference:author" ],
                  "value" : [ "user" ]
                }
              },
              { "data_set" : {
                  "data" : [
                    "line-item:field-node-reference:field-node-reference:field-quote-order:owner"
                  ],
                  "value" : [ "user" ]
                }
              },
              { "data_set" : {
                  "data" : [
                    "line-item:field-node-reference:field-node-reference:field-quote-order:mail"
                  ],
                  "value" : [ "user:mail" ]
                }
              }
            ],
            "LABEL" : "Estimate"
          }
        },
        { "RULE" : {
            "IF" : [
              { "entity_has_field" : { "entity" : [ "line-item" ], "field" : "field_node_reference" } },
              { "NOT data_is_empty" : { "data" : [ "line-item:field-node-reference" ] } }
            ],
            "DO" : [
              { "data_set" : {
                  "data" : [ "line-item:field-node-reference:author" ],
                  "value" : [ "user" ]
                }
              }
            ],
            "LABEL" : "Website or Domain Name"
          }
        }
      ]
    }
  }');
  $items['rules_payment_complete'] = entity_import('rules_config', '{ "rules_payment_complete" : {
      "LABEL" : "Payment Complete",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "HD Webstore" ],
      "REQUIRES" : [ "rules", "commerce_checkout", "commerce_payment" ],
      "ON" : [ "commerce_payment_order_paid_in_full" ],
      "DO" : [
        { "LOOP" : {
            "USING" : { "list" : [ "commerce-order:commerce-line-items" ] },
            "ITEM" : { "line_item" : "Line Item" },
            "DO" : [
              { "component_rules_process_purchases" : { "line_item" : [ "line_item" ] } }
            ]
          }
        },
        { "commerce_checkout_complete" : { "commerce_order" : [ "commerce_order" ] } },
        { "data_set" : { "data" : [ "commerce-order:status" ], "value" : "completed" } }
      ]
    }
  }');
  $items['rules_payment_return'] = entity_import('rules_config', '{ "rules_payment_return" : {
      "LABEL" : "Payment Return",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "HD Webstore" ],
      "REQUIRES" : [ "pathrules", "rules", "commerce_rules_extra" ],
      "ON" : [ "process_checkout_pane" ],
      "IF" : [
        { "pathrules_checkpath" : { "path" : "[^=]checkout\\/[0-9]+\\/complete", "operation" : "regex" } }
      ],
      "DO" : [
        { "component_rules_process_payment_return" : {
            "user" : [ "site:current-user" ],
            "order" : [ "site:current-cart-order" ]
          }
        }
      ]
    }
  }');
  $items['rules_process_checkout'] = entity_import('rules_config', '{ "rules_process_checkout" : {
      "LABEL" : "Process Checkout",
      "PLUGIN" : "rule set",
      "TAGS" : [ "HD Webstore" ],
      "REQUIRES" : [ "commerce_order", "rules", "commerce_rules_extra" ],
      "USES VARIABLES" : {
        "order" : { "label" : "Order", "type" : "commerce_order" },
        "user" : { "label" : "User", "type" : "user" }
      },
      "RULES" : [
        { "RULE" : {
            "IF" : [
              { "commerce_order_contains_product" : {
                  "commerce_order" : [ "order" ],
                  "product_id" : "ESTIMATE",
                  "operator" : "\\u003E=",
                  "value" : "1"
                }
              },
              { "NOT commerce_order_contains_product_type" : {
                  "commerce_order" : [ "order" ],
                  "product_type" : { "value" : { "commerce_sp_subscription" : "commerce_sp_subscription" } },
                  "operator" : "\\u003E=",
                  "value" : "1"
                }
              },
              { "NOT data_is" : {
                  "data" : [ "order:commerce-order-total:amount" ],
                  "op" : "\\u003E",
                  "value" : "0"
                }
              }
            ],
            "DO" : [
              { "LOOP" : {
                  "USING" : { "list" : [ "order:commerce-line-items" ] },
                  "ITEM" : { "line_item" : "Line Item" },
                  "DO" : [
                    { "component_rules_estimate_only" : { "line_item" : [ "line_item" ] } }
                  ]
                }
              }
            ],
            "LABEL" : "Only Estimate"
          }
        },
        { "RULE" : {
            "PROVIDE" : { "entity_created" : { "product_order" : "Product Order" } },
            "IF" : [
              { "commerce_order_contains_product_type" : {
                  "commerce_order" : [ "order" ],
                  "product_type" : { "value" : { "product" : "product" } },
                  "operator" : "\\u003E=",
                  "value" : "1"
                }
              },
              { "commerce_order_contains_product_type" : {
                  "commerce_order" : [ "order" ],
                  "product_type" : { "value" : { "commerce_sp_subscription" : "commerce_sp_subscription" } },
                  "operator" : "\\u003E=",
                  "value" : "1"
                }
              },
              { "entity_has_field" : { "entity" : [ "order" ], "field" : "field_next_order" } }
            ],
            "DO" : [
              { "entity_create" : {
                  "USING" : {
                    "type" : "commerce_order",
                    "param_status" : "pending",
                    "param_type" : "commerce_order",
                    "param_owner" : [ "user" ],
                    "param_commerce_order_total" : { "value" : { "amount" : 100, "currency_code" : "USD" } }
                  },
                  "PROVIDE" : { "entity_created" : { "product_order" : "Product Order" } }
                }
              },
              { "data_set" : { "data" : [ "product-order:mail" ], "value" : [ "order:mail" ] } },
              { "entity_save" : { "data" : [ "product-order" ], "immediate" : 1 } },
              { "data_set" : { "data" : [ "order:field-next-order" ], "value" : [ "product-order" ] } },
              { "data_set" : { "data" : [ "user:field-user-order-stage" ], "value" : "product" } },
              { "LOOP" : {
                  "USING" : { "list" : [ "order:commerce-line-items" ] },
                  "ITEM" : { "line_item" : "Line item" },
                  "DO" : [
                    { "component_rules_seperate_products_from_order" : {
                        "line_item" : [ "line_item" ],
                        "product_order" : [ "product_order" ],
                        "base_order" : [ "order" ]
                      }
                    }
                  ]
                }
              }
            ],
            "LABEL" : "Product \\u0026 Subscription"
          }
        },
        { "RULE" : {
            "IF" : [
              { "commerce_order_contains_product" : {
                  "commerce_order" : [ "order" ],
                  "product_id" : "ESTIMATE",
                  "operator" : "\\u003E=",
                  "value" : "1"
                }
              },
              { "commerce_order_contains_product_type" : {
                  "commerce_order" : [ "order" ],
                  "product_type" : { "value" : { "commerce_sp_subscription" : "commerce_sp_subscription" } },
                  "operator" : "\\u003E=",
                  "value" : "1"
                }
              },
              { "commerce_rules_extra_compare_total_product_type_quantity" : {
                  "commerce_order" : [ "order" ],
                  "product_type" : "product",
                  "exclude" : 0,
                  "operator" : "=",
                  "value" : "1"
                }
              }
            ],
            "DO" : [
              { "LOOP" : {
                  "USING" : { "list" : [ "order:commerce-line-items" ] },
                  "ITEM" : { "line_item" : "Line Item" },
                  "DO" : [
                    { "component_rules_estimate_plus" : { "line_item" : [ "line_item" ], "product_order" : [ "order" ] } }
                  ]
                }
              },
              { "redirect" : { "url" : "checkout" } }
            ],
            "LABEL" : "Estimate \\u0026 Subscription"
          }
        },
        { "RULE" : {
            "IF" : [
              { "commerce_order_contains_product_type" : {
                  "commerce_order" : [ "order" ],
                  "product_type" : { "value" : { "product" : "product" } },
                  "operator" : "\\u003E=",
                  "value" : "1"
                }
              },
              { "commerce_order_contains_product" : {
                  "commerce_order" : [ "order" ],
                  "product_id" : "ESTIMATE",
                  "operator" : "\\u003E=",
                  "value" : "1"
                }
              },
              { "data_is" : {
                  "data" : [ "order:commerce-order-total:amount" ],
                  "op" : "\\u003E",
                  "value" : "0"
                }
              },
              { "entity_has_field" : { "entity" : [ "order" ], "field" : "field_next_order" } }
            ],
            "DO" : [
              { "data_set" : { "data" : [ "user:field-user-order-stage" ], "value" : "quote" } },
              { "LOOP" : {
                  "USING" : { "list" : [ "order:commerce-line-items" ] },
                  "ITEM" : { "line_item" : "Line Item" },
                  "DO" : [
                    { "component_rules_estimate_plus" : { "line_item" : [ "line_item" ], "product_order" : [ "order" ] } }
                  ]
                }
              }
            ],
            "LABEL" : "Product \\u0026 Estimate"
          }
        }
      ]
    }
  }');
  $items['rules_process_payment_return'] = entity_import('rules_config', '{ "rules_process_payment_return" : {
      "LABEL" : "Process Payment Return",
      "PLUGIN" : "rule set",
      "TAGS" : [ "HD Webstore" ],
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : {
        "user" : { "label" : "User", "type" : "user" },
        "order" : { "label" : "Order", "type" : "commerce_order" }
      },
      "RULES" : [
        { "RULE" : {
            "IF" : [
              { "OR" : [
                  { "data_is" : { "data" : [ "user:field-user-order-stage" ], "value" : "complete" } },
                  { "data_is_empty" : { "data" : [ "user:field-user-order-stage" ] } }
                ]
              }
            ],
            "DO" : [
              { "redirect" : { "url" : "portal\\/orders" } },
              { "drupal_message" : { "message" : "Thank you for your order." } }
            ],
            "LABEL" : "Complete"
          }
        },
        { "RULE" : {
            "IF" : [
              { "data_is" : { "data" : [ "user:field-user-order-stage" ], "value" : "product" } }
            ],
            "DO" : [
              { "redirect" : { "url" : "checkout" } },
              { "data_set" : { "data" : [ "user:field-user-order-stage" ], "value" : "complete" } }
            ],
            "LABEL" : "Product Pending"
          }
        },
        { "RULE" : {
            "IF" : [
              { "data_is" : { "data" : [ "user:field-user-order-stage" ], "value" : "quote" } }
            ],
            "DO" : [
              { "redirect" : { "url" : "portal\\/quotes" } },
              { "data_set" : { "data" : [ "user:field-user-order-stage" ], "value" : "complete" } },
              { "drupal_message" : { "message" : "Thank you for your project request, we have received it and will reply with an estimate shortly." } }
            ],
            "LABEL" : "Estimate Pending"
          }
        }
      ]
    }
  }');
  $items['rules_process_purchases'] = entity_import('rules_config', '{ "rules_process_purchases" : {
      "LABEL" : "Process Purchases",
      "PLUGIN" : "rule set",
      "TAGS" : [ "HD Webstore" ],
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "line_item" : { "label" : "Line Item", "type" : "commerce_line_item" } },
      "RULES" : [
        { "RULE" : {
            "IF" : [
              { "entity_has_field" : { "entity" : [ "line-item" ], "field" : "commerce_product" } },
              { "data_is" : {
                  "data" : [ "line-item:commerce-product:type" ],
                  "value" : "commerce_sp_subscription"
                }
              }
            ],
            "DO" : [],
            "LABEL" : "Website"
          }
        },
        { "RULE" : {
            "IF" : [
              { "entity_has_field" : { "entity" : [ "line-item" ], "field" : "commerce_product" } },
              { "data_is" : { "data" : [ "line-item:commerce-product:sku" ], "value" : "ESTIMATE" } }
            ],
            "DO" : [],
            "LABEL" : "Estimate"
          }
        },
        { "RULE" : { "DO" : [], "LABEL" : "Domain Name" } }
      ]
    }
  }');
  $items['rules_project_form_create'] = entity_import('rules_config', '{ "rules_project_form_create" : {
      "LABEL" : "Project Form Create",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "HD Webstore" ],
      "REQUIRES" : [ "rules", "commerce_cart" ],
      "ON" : [ "node_insert" ],
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "project_form" : "project_form" } }
          }
        }
      ],
      "DO" : [
        { "commerce_cart_product_add_by_sku" : {
            "USING" : {
              "user" : [ "site:current-user" ],
              "sku" : "ESTIMATE",
              "quantity" : "1",
              "combine" : 1
            },
            "PROVIDE" : { "product_add_line_item" : { "estimate_in_cart" : "Estimate in cart" } }
          }
        },
        { "component_rules_set_line_item_labels" : { "line_item" : [ "estimate-in-cart" ], "node" : [ "node" ] } },
        { "component_rules_reference_line_item_to_node" : { "line_item" : [ "estimate-in-cart" ], "node" : [ "node" ] } },
        { "entity_create" : {
            "USING" : {
              "type" : "node",
              "param_type" : "estimate",
              "param_title" : "Estimate [estimate-created:order-number]",
              "param_author" : [ "site:current-user" ]
            },
            "PROVIDE" : { "entity_created" : { "estimate_node_created" : "Created Estimate Node" } }
          }
        },
        { "entity_save" : { "data" : [ "estimate-node-created" ], "immediate" : 1 } },
        { "entity_create" : {
            "USING" : {
              "type" : "commerce_order",
              "param_status" : "pending",
              "param_type" : "commerce_order",
              "param_owner" : [ "site:current-user" ],
              "param_commerce_order_total" : { "value" : { "amount" : 100, "currency_code" : "USD" } }
            },
            "PROVIDE" : { "entity_created" : { "estimate_order_created" : "Estimate Order" } }
          }
        },
        { "entity_save" : { "data" : [ "estimate-order-created" ], "immediate" : 1 } },
        { "data_set" : {
            "data" : [ "node:field-node-reference" ],
            "value" : [ "estimate-node-created" ]
          }
        },
        { "data_set" : {
            "data" : [ "estimate-node-created:field-quote-order" ],
            "value" : [ "estimate-order-created" ]
          }
        },
        { "data_set" : {
            "data" : [ "estimate-order-created:field-node-reference" ],
            "value" : [ "estimate-node-created" ]
          }
        },
        { "data_set" : {
            "data" : [ "estimate-node-created:field-node-reference" ],
            "value" : [ "node" ]
          }
        },
        { "component_rules_project_form_process" : { "project_form" : [ "node" ], "estimate" : [ "estimate-order-created" ] } },
        { "redirect" : { "url" : "cart" } }
      ]
    }
  }');
  $items['rules_project_form_process'] = entity_import('rules_config', '{ "rules_project_form_process" : {
      "LABEL" : "Project Form Process",
      "PLUGIN" : "rule set",
      "TAGS" : [ "HD Webstore" ],
      "REQUIRES" : [ "rules", "commerce_cart" ],
      "USES VARIABLES" : {
        "project_form" : { "label" : "Project Form", "type" : "node" },
        "estimate" : { "label" : "Estimate", "type" : "commerce_order" }
      },
      "RULES" : [
        { "RULE" : {
            "PROVIDE" : { "product_add_line_item" : { "logo_in_cart" : "Logo in cart" } },
            "IF" : [
              { "node_is_of_type" : {
                  "node" : [ "project-form" ],
                  "type" : { "value" : { "project_form" : "project_form" } }
                }
              },
              { "data_is" : { "data" : [ "project-form:field-bool-logo" ], "value" : 1 } }
            ],
            "DO" : [
              { "commerce_cart_product_add_by_sku" : {
                  "USING" : {
                    "user" : [ "project-form:author" ],
                    "sku" : "LOGO-DESIGN",
                    "quantity" : "1",
                    "combine" : 1
                  },
                  "PROVIDE" : { "product_add_line_item" : { "logo_in_cart" : "Logo in cart" } }
                }
              },
              { "list_add" : {
                  "list" : [ "estimate:commerce-line-items" ],
                  "item" : [ "logo-in-cart" ]
                }
              },
              { "list_remove" : {
                  "list" : [ "site:current-cart-order:commerce-line-items" ],
                  "item" : [ "logo-in-cart" ]
                }
              }
            ],
            "LABEL" : "Logo is Chosen"
          }
        }
      ]
    }
  }');
  $items['rules_project_form_update'] = entity_import('rules_config', '{ "rules_project_form_update" : {
      "LABEL" : "Project Form Update",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "HD Webstore" ],
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_update" ],
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "project_form" : "project_form" } }
          }
        },
        { "entity_has_field" : { "entity" : [ "node" ], "field" : "field_line_item_reference" } }
      ],
      "DO" : [
        { "component_rules_set_line_item_labels" : {
            "line_item" : [ "node:field-line-item-reference" ],
            "node" : [ "node" ]
          }
        }
      ]
    }
  }');
  $items['rules_quote_approve'] = entity_import('rules_config', '{ "rules_quote_approve" : {
      "LABEL" : "Quote Approve",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "HD Webstore" ],
      "REQUIRES" : [ "rules", "commerce_quotes" ],
      "ON" : [ "commerce_quotes_accepted" ],
      "DO" : [
        { "data_set" : { "data" : [ "commerce-order:status" ], "value" : "cart" } },
        { "redirect" : { "url" : "checkout" } }
      ]
    }
  }');
  $items['rules_quote_deny'] = entity_import('rules_config', '{ "rules_quote_deny" : {
      "LABEL" : "Quote Deny",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "HD Webstore" ],
      "REQUIRES" : [ "mimemail", "commerce_quotes" ],
      "ON" : [ "commerce_quotes_declined" ],
      "DO" : [
        { "mimemail" : {
            "to" : [ "commerce-order:mail" ],
            "subject" : "I\\u0027m sorry you didn\\u0027t like the estimate, what can we do",
            "body" : "You recently declined on a proposal we submitted to you, can we talk about why you weren\\u0027t happy with it?"
          }
        }
      ]
    }
  }');
  $items['rules_quote_response'] = entity_import('rules_config', '{ "rules_quote_response" : {
      "LABEL" : "Quote Response",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "HD Webstore" ],
      "REQUIRES" : [ "rules", "mimemail", "entity" ],
      "ON" : [ "commerce_order_update" ],
      "IF" : [
        { "data_is" : { "data" : [ "commerce-order:status" ], "value" : "quotation_response" } }
      ],
      "DO" : [
        { "mimemail" : {
            "to" : [ "commerce-order:mail" ],
            "subject" : "Your estimate ",
            "body" : "Here is the proposal for the project you requested:\\r\\n\\u003Ca href=\\u0022[site:url]portal\\/quote\\/[commerce-order:order-number]\\u0022\\u003EEstimate [commerce-order:order-number]\\u003C\\/a\\u003E"
          }
        }
      ]
    }
  }');
  $items['rules_reference_line_item_to_node'] = entity_import('rules_config', '{ "rules_reference_line_item_to_node" : {
      "LABEL" : "Reference Line Item to Node",
      "PLUGIN" : "rule",
      "TAGS" : [ "HD Webstore" ],
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : {
        "line_item" : { "label" : "Line Item", "type" : "commerce_line_item" },
        "node" : { "label" : "Node", "type" : "node" }
      },
      "IF" : [
        { "entity_has_field" : { "entity" : [ "line-item" ], "field" : "field_node_reference" } },
        { "entity_has_field" : { "entity" : [ "node" ], "field" : "field_line_item_reference" } }
      ],
      "DO" : [
        { "data_set" : { "data" : [ "line-item:field-node-reference" ], "value" : [ "node" ] } },
        { "data_set" : {
            "data" : [ "node:field-line-item-reference" ],
            "value" : [ "line-item" ]
          }
        }
      ]
    }
  }');
  $items['rules_seperate_products_from_order'] = entity_import('rules_config', '{ "rules_seperate_products_from_order" : {
      "LABEL" : "Seperate Products from Order",
      "PLUGIN" : "rule",
      "TAGS" : [ "HD Webstore" ],
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : {
        "line_item" : { "label" : "Line Item", "type" : "commerce_line_item" },
        "product_order" : { "label" : "Product Order", "type" : "commerce_order" },
        "base_order" : { "label" : "Base Order", "type" : "commerce_order" }
      },
      "IF" : [
        { "entity_has_field" : { "entity" : [ "line-item" ], "field" : "commerce_product" } },
        { "data_is" : { "data" : [ "line-item:commerce-product:type" ], "value" : "product" } }
      ],
      "DO" : [
        { "list_add" : {
            "list" : [ "product-order:commerce-line-items" ],
            "item" : [ "line-item" ]
          }
        },
        { "list_remove" : {
            "list" : [ "base-order:commerce-line-items" ],
            "item" : [ "line-item" ]
          }
        },
        { "drupal_message" : { "message" : "We\\u0027ve moved your non-subscription products to a separate order that you will checkout after your subscription." } }
      ]
    }
  }');
  $items['rules_set_line_item_label_1'] = entity_import('rules_config', '{ "rules_set_line_item_label_1" : {
      "LABEL" : "Set Line Item Label 1",
      "PLUGIN" : "rule",
      "TAGS" : [ "HD Webstore" ],
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : {
        "line_item" : { "label" : "Line Item", "type" : "commerce_line_item" },
        "label_1" : { "label" : "Label 1", "type" : "text" }
      },
      "IF" : [
        { "entity_has_field" : { "entity" : [ "line-item" ], "field" : "field_li_info_1" } }
      ],
      "DO" : [
        { "data_set" : { "data" : [ "line-item:field-li-info-1" ], "value" : [ "label-1" ] } }
      ]
    }
  }');
  $items['rules_set_line_item_label_2'] = entity_import('rules_config', '{ "rules_set_line_item_label_2" : {
      "LABEL" : "Set Line Item Label 2",
      "PLUGIN" : "rule",
      "TAGS" : [ "HD Webstore" ],
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : {
        "line_item" : { "label" : "Line Item", "type" : "commerce_line_item" },
        "label_2" : { "label" : "Label 2", "type" : "text" }
      },
      "IF" : [
        { "entity_has_field" : { "entity" : [ "line-item" ], "field" : "field_li_info_2" } }
      ],
      "DO" : [
        { "data_set" : { "data" : [ "line-item:field-li-info-2" ], "value" : [ "label-2" ] } }
      ]
    }
  }');
  $items['rules_set_line_item_labels'] = entity_import('rules_config', '{ "rules_set_line_item_labels" : {
      "LABEL" : "Set Line Item Labels",
      "PLUGIN" : "rule set",
      "TAGS" : [ "HD Webstore" ],
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : {
        "line_item" : { "label" : "Line Item", "type" : "commerce_line_item" },
        "node" : { "label" : "Node", "type" : "node" }
      },
      "RULES" : [
        { "RULE" : {
            "IF" : [
              { "node_is_of_type" : {
                  "node" : [ "node" ],
                  "type" : { "value" : { "website_form" : "website_form" } }
                }
              }
            ],
            "DO" : [
              { "component_rules_set_line_item_label_1" : {
                  "line_item" : [ "line_item" ],
                  "label_1" : [ "node:field-website-name" ]
                }
              },
              { "component_rules_set_line_item_label_2" : {
                  "line_item" : [ "line_item" ],
                  "label_2" : [ "node:field-website-subdomain" ]
                }
              }
            ],
            "LABEL" : "Website Form"
          }
        },
        { "RULE" : {
            "IF" : [
              { "node_is_of_type" : {
                  "node" : [ "node" ],
                  "type" : { "value" : { "project_form" : "project_form" } }
                }
              }
            ],
            "DO" : [
              { "component_rules_set_line_item_label_1" : {
                  "line_item" : [ "line_item" ],
                  "label_1" : [ "line-item:order:order-number" ]
                }
              }
            ],
            "LABEL" : "Project Form"
          }
        },
        { "RULE" : {
            "IF" : [
              { "node_is_of_type" : {
                  "node" : [ "node" ],
                  "type" : { "value" : { "domain_form" : "domain_form" } }
                }
              }
            ],
            "DO" : [
              { "component_rules_set_line_item_label_1" : {
                  "line_item" : [ "line_item" ],
                  "label_1" : "[node:field-domain-name-prefix][node:field-domain-name-suffix]"
                }
              }
            ],
            "LABEL" : "Domain Form"
          }
        }
      ]
    }
  }');
  $items['rules_website_form_create'] = entity_import('rules_config', '{ "rules_website_form_create" : {
      "LABEL" : "Website Form Create",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "HD Webstore" ],
      "REQUIRES" : [ "rules", "commerce_cart" ],
      "ON" : [ "node_insert" ],
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "website_form" : "website_form" } }
          }
        }
      ],
      "DO" : [
        { "commerce_cart_product_add_by_sku" : {
            "USING" : {
              "user" : [ "node:author" ],
              "sku" : "WEBSITE",
              "quantity" : "1",
              "combine" : 1
            },
            "PROVIDE" : { "product_add_line_item" : { "website_in_cart" : "Website in cart" } }
          }
        },
        { "component_rules_reference_line_item_to_node" : { "line_item" : [ "website-in-cart" ], "node" : [ "node" ] } },
        { "component_rules_set_line_item_labels" : { "line_item" : [ "website-in-cart" ] } },
        { "redirect" : { "url" : "cart" } }
      ]
    }
  }');
  $items['rules_website_form_update'] = entity_import('rules_config', '{ "rules_website_form_update" : {
      "LABEL" : "Website Form Update",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "HD Webstore" ],
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_update" ],
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "website_form" : "website_form" } }
          }
        },
        { "entity_has_field" : { "entity" : [ "node" ], "field" : "field_line_item_reference" } }
      ],
      "DO" : [
        { "component_rules_set_line_item_labels" : { "line_item" : [ "node:field-line-item-reference" ] } }
      ]
    }
  }');
  return $items;
}
