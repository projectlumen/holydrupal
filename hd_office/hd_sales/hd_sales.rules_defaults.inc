<?php
/**
 * @file
 * hd_sales.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function hd_sales_default_rules_configuration() {
  $items = array();
  $items['rules_company_branch'] = entity_import('rules_config', '{ "rules_company_branch" : {
      "LABEL" : "Company Branch",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "CRM" ],
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_insert" ],
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "company_branch" : "company_branch" } }
          }
        }
      ],
      "DO" : [
        { "component_rules_company_branch_relations" : { "cobran" : [ "node" ] } }
      ]
    }
  }');
  $items['rules_company_branch_on_import'] = entity_import('rules_config', '{ "rules_company_branch_on_import" : {
      "LABEL" : "Company Branch on Import",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "CRM" ],
      "REQUIRES" : [ "rules", "feeds" ],
      "ON" : [ "feeds_import_company_import" ],
      "IF" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "company" : "company" } } } },
        { "NOT data_is_empty" : { "data" : [ "node:field-company-primary" ] } }
      ],
      "DO" : [
        { "drupal_message" : { "message" : [ "node:field-company-primary:title" ] } },
        { "data_set" : { "data" : [ "node:field-company-fax" ], "value" : "This is a branch" } }
      ]
    }
  }');
  $items['rules_company_branch_relations'] = entity_import('rules_config', '{ "rules_company_branch_relations" : {
      "LABEL" : "Company Branch Relations",
      "PLUGIN" : "rule set",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "cobran" : { "label" : "Company Branch", "type" : "node" } },
      "RULES" : [
        { "RULE" : {
            "PROVIDE" : {
              "entity_created" : {
                "branch_created" : "Created branch",
                "cobran_relation" : "CoBran Relation"
              },
              "variable_added" : { "cobran_list" : "CoBran List" }
            },
            "IF" : [
              { "node_is_of_type" : {
                  "node" : [ "cobran" ],
                  "type" : { "value" : { "company_branch" : "company_branch" } }
                }
              }
            ],
            "DO" : [
              { "entity_create" : {
                  "USING" : {
                    "type" : "node",
                    "param_type" : "company",
                    "param_title" : [ "cobran:title" ],
                    "param_author" : [ "cobran:author" ]
                  },
                  "PROVIDE" : { "entity_created" : { "branch_created" : "Created branch" } }
                }
              },
              { "data_set" : { "data" : [ "branch-created:field-company-branch" ], "value" : 1 } },
              { "data_set" : {
                  "data" : [ "branch-created:field-company-primary" ],
                  "value" : [ "cobran:field-cobran-primary-company" ]
                }
              },
              { "entity_save" : { "data" : [ "branch-created" ], "immediate" : 1 } },
              { "data_set" : {
                  "data" : [ "cobran:field-cobran-branch-company" ],
                  "value" : [ "branch-created" ]
                }
              },
              { "variable_add" : {
                  "USING" : { "type" : "list\\u003Centity\\u003E", "value" : [ "" ] },
                  "PROVIDE" : { "variable_added" : { "cobran_list" : "CoBran List" } }
                }
              },
              { "list_add" : {
                  "list" : [ "cobran-list" ],
                  "item" : [ "cobran:field-cobran-primary-company" ]
                }
              },
              { "list_add" : {
                  "list" : [ "cobran-list" ],
                  "item" : [ "cobran:field-cobran-branch-company" ]
                }
              },
              { "entity_create" : {
                  "USING" : {
                    "type" : "relation",
                    "param_relation_type" : "branch_relation",
                    "param_endpoints" : [ "cobran-list" ]
                  },
                  "PROVIDE" : { "entity_created" : { "cobran_relation" : "CoBran Relation" } }
                }
              },
              { "entity_delete" : { "data" : [ "cobran" ] } },
              { "redirect" : { "url" : [ "cobran:field-cobran-primary-company:url" ] } }
            ],
            "LABEL" : "New Branch"
          }
        }
      ]
    }
  }');
  $items['rules_company_contact'] = entity_import('rules_config', '{ "rules_company_contact" : {
      "LABEL" : "Company Contact Relations",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "CRM" ],
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_insert" ],
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "company_contact" : "company_contact" } }
          }
        }
      ],
      "DO" : [
        { "component_rules_company_contact_relations" : { "coco" : [ "node" ] } }
      ]
    }
  }');
  $items['rules_company_contact_relations'] = entity_import('rules_config', '{ "rules_company_contact_relations" : {
      "LABEL" : "Company Contact Relations",
      "PLUGIN" : "rule set",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "coco" : { "label" : "Company Contact", "type" : "node" } },
      "RULES" : [
        { "RULE" : {
            "PROVIDE" : {
              "entity_created" : {
                "contact_created" : "Created contact",
                "relation_created" : "Created relation"
              },
              "variable_added" : { "coco_list" : "CoCo List" }
            },
            "IF" : [
              { "node_is_of_type" : {
                  "node" : [ "coco" ],
                  "type" : { "value" : { "company_contact" : "company_contact" } }
                }
              },
              { "data_is" : { "data" : [ "coco:field-coco-create-new" ], "value" : 1 } }
            ],
            "DO" : [
              { "entity_create" : {
                  "USING" : {
                    "type" : "node",
                    "param_type" : "contact",
                    "param_title" : "temp-name-[coco:nid]",
                    "param_author" : [ "coco:author" ]
                  },
                  "PROVIDE" : { "entity_created" : { "contact_created" : "Created contact" } }
                }
              },
              { "entity_save" : { "data" : [ "contact-created" ], "immediate" : 1 } },
              { "data_set" : {
                  "data" : [ "contact-created:field-contact-first-name" ],
                  "value" : [ "coco:field-coco-first-name" ]
                }
              },
              { "data_set" : {
                  "data" : [ "contact-created:field-contact-last-name" ],
                  "value" : [ "coco:field-coco-last-name" ]
                }
              },
              { "data_set" : {
                  "data" : [ "coco:field-coco-contact" ],
                  "value" : [ "contact-created" ]
                }
              },
              { "variable_add" : {
                  "USING" : { "type" : "list\\u003Centity\\u003E", "value" : [ "" ] },
                  "PROVIDE" : { "variable_added" : { "coco_list" : "CoCo List" } }
                }
              },
              { "list_add" : { "list" : [ "coco-list" ], "item" : [ "coco:field-coco-company" ] } },
              { "list_add" : { "list" : [ "coco-list" ], "item" : [ "coco:field-coco-contact" ] } },
              { "entity_create" : {
                  "USING" : {
                    "type" : "relation",
                    "param_relation_type" : "company_contact",
                    "param_endpoints" : [ "coco-list" ]
                  },
                  "PROVIDE" : { "entity_created" : { "relation_created" : "Created relation" } }
                }
              },
              { "data_set" : {
                  "data" : [ "relation-created:field-relation-company-position" ],
                  "value" : [ "coco:field-coco-company-position" ]
                }
              },
              { "data_set" : {
                  "data" : [ "contact-created:title" ],
                  "value" : "[contact-created:field-contact-first-name] [contact-created:field-contact-last-name]"
                }
              },
              { "entity_delete" : { "data" : [ "coco" ] } },
              { "redirect" : { "url" : [ "coco:field-coco-company:url" ] } }
            ],
            "LABEL" : "New Contact"
          }
        },
        { "RULE" : {
            "PROVIDE" : {
              "variable_added" : { "old_list" : "Old List" },
              "entity_created" : { "old_relation" : "Old Relation" }
            },
            "IF" : [
              { "node_is_of_type" : {
                  "node" : [ "coco" ],
                  "type" : { "value" : { "company_contact" : "company_contact" } }
                }
              },
              { "data_is" : { "data" : [ "coco:field-coco-create-new" ], "value" : 0 } }
            ],
            "DO" : [
              { "variable_add" : {
                  "USING" : { "type" : "list\\u003Centity\\u003E", "value" : [ "" ] },
                  "PROVIDE" : { "variable_added" : { "old_list" : "Old List" } }
                }
              },
              { "list_add" : { "list" : [ "old-list" ], "item" : [ "coco:field-coco-company" ] } },
              { "list_add" : { "list" : [ "old-list" ], "item" : [ "coco:field-coco-contact" ] } },
              { "entity_create" : {
                  "USING" : {
                    "type" : "relation",
                    "param_relation_type" : "company_contact",
                    "param_endpoints" : [ "old-list" ]
                  },
                  "PROVIDE" : { "entity_created" : { "old_relation" : "Old Relation" } }
                }
              },
              { "data_set" : {
                  "data" : [ "old-relation:field-relation-company-position" ],
                  "value" : [ "coco:field-coco-company-position" ]
                }
              },
              { "entity_delete" : { "data" : [ "coco" ] } },
              { "redirect" : { "url" : [ "coco:field-coco-company:url" ] } }
            ],
            "LABEL" : "Old Contact"
          }
        }
      ]
    }
  }');
  $items['rules_company_import'] = entity_import('rules_config', '{ "rules_company_import" : {
      "LABEL" : "Company Import",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "CRM" ],
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_view" ],
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "company_import" : "company_import" } }
          }
        }
      ],
      "DO" : [
        { "redirect" : { "url" : "leads" } },
        { "entity_delete" : { "data" : [ "node" ] } }
      ]
    }
  }');
  $items['rules_contact_company_relation'] = entity_import('rules_config', '{ "rules_contact_company_relation" : {
      "LABEL" : "Contact Company Relations",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "CRM" ],
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_insert" ],
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "contact_company" : "contact_company" } }
          }
        }
      ],
      "DO" : [
        { "component_rules_contact_company_relations" : { "conco" : [ "node" ] } }
      ]
    }
  }');
  $items['rules_contact_company_relations'] = entity_import('rules_config', '{ "rules_contact_company_relations" : {
      "LABEL" : "Contact Company Relations",
      "PLUGIN" : "rule set",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "conco" : { "label" : "Conco", "type" : "node" } },
      "RULES" : [
        { "RULE" : {
            "PROVIDE" : {
              "entity_created" : { "company_created" : "Created company", "new_relation" : "New Relation" },
              "variable_added" : { "new_list" : "New List" }
            },
            "IF" : [
              { "node_is_of_type" : {
                  "node" : [ "conco" ],
                  "type" : { "value" : { "contact_company" : "contact_company" } }
                }
              },
              { "data_is" : { "data" : [ "conco:field-conco-new-company" ], "value" : 1 } }
            ],
            "DO" : [
              { "entity_create" : {
                  "USING" : {
                    "type" : "node",
                    "param_type" : "company",
                    "param_title" : [ "conco:field-conco-company-name" ],
                    "param_author" : [ "conco:author" ]
                  },
                  "PROVIDE" : { "entity_created" : { "company_created" : "Created company" } }
                }
              },
              { "entity_save" : { "data" : [ "company-created" ], "immediate" : 1 } },
              { "data_set" : {
                  "data" : [ "conco:field-conco-company" ],
                  "value" : [ "company-created" ]
                }
              },
              { "variable_add" : {
                  "USING" : { "type" : "list\\u003Centity\\u003E", "value" : [ "" ] },
                  "PROVIDE" : { "variable_added" : { "new_list" : "New List" } }
                }
              },
              { "list_add" : { "list" : [ "new-list" ], "item" : [ "conco:field-conco-company" ] } },
              { "list_add" : { "list" : [ "new-list" ], "item" : [ "conco:field-conco-contact" ] } },
              { "entity_create" : {
                  "USING" : {
                    "type" : "relation",
                    "param_relation_type" : "company_contact",
                    "param_endpoints" : [ "new-list" ]
                  },
                  "PROVIDE" : { "entity_created" : { "new_relation" : "New Relation" } }
                }
              },
              { "data_set" : {
                  "data" : [ "new-relation:field-relation-company-position" ],
                  "value" : [ "conco:field-conco-company-position" ]
                }
              },
              { "data_set" : {
                  "data" : [ "company-created:field-company-type" ],
                  "value" : [ "conco:field-conco-company-type" ]
                }
              },
              { "data_set" : {
                  "data" : [ "company-created:field-company-status" ],
                  "value" : [ "conco:field-conco-company-status" ]
                }
              },
              { "entity_delete" : { "data" : [ "conco" ] } },
              { "redirect" : { "url" : [ "conco:field-conco-contact:url" ] } }
            ],
            "LABEL" : "New Company"
          }
        },
        { "RULE" : {
            "PROVIDE" : {
              "variable_added" : { "old_list" : "Old List" },
              "entity_created" : { "old_relation" : "Old Relation" }
            },
            "IF" : [
              { "node_is_of_type" : {
                  "node" : [ "conco" ],
                  "type" : { "value" : { "contact_company" : "contact_company" } }
                }
              },
              { "data_is" : { "data" : [ "conco:field-conco-new-company" ], "value" : 0 } }
            ],
            "DO" : [
              { "variable_add" : {
                  "USING" : { "type" : "list\\u003Centity\\u003E", "value" : [ "" ] },
                  "PROVIDE" : { "variable_added" : { "old_list" : "Old List" } }
                }
              },
              { "list_add" : { "list" : [ "old-list" ], "item" : [ "conco:field-conco-company" ] } },
              { "list_add" : { "list" : [ "old-list" ], "item" : [ "conco:field-conco-contact" ] } },
              { "entity_create" : {
                  "USING" : {
                    "type" : "relation",
                    "param_relation_type" : "company_contact",
                    "param_endpoints" : [ "old-list" ]
                  },
                  "PROVIDE" : { "entity_created" : { "old_relation" : "Old Relation" } }
                }
              },
              { "data_set" : {
                  "data" : [ "old-relation:field-relation-company-position" ],
                  "value" : [ "conco:field-conco-company-position" ]
                }
              },
              { "entity_delete" : { "data" : [ "conco" ] } },
              { "redirect" : { "url" : [ "conco:field-conco-contact:url" ] } }
            ],
            "LABEL" : "Old Company"
          }
        }
      ]
    }
  }');
  return $items;
}
