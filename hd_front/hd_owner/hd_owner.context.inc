<?php
/**
 * @file
 * hd_owner.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function hd_owner_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'owner_conditional';
  $context->description = '';
  $context->tag = 'HD Client';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '~contact' => '~contact',
      ),
    ),
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'boxes-contact_box' => array(
          'module' => 'boxes',
          'delta' => 'contact_box',
          'region' => 'sidebar_right',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('HD Client');
  $export['owner_conditional'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'owner_sitewide';
  $context->description = '';
  $context->tag = 'HD Client';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'system-main-menu' => array(
          'module' => 'system',
          'delta' => 'main-menu',
          'region' => 'navigation',
          'weight' => '-10',
        ),
        'panels_mini-footer_mini_panel' => array(
          'module' => 'panels_mini',
          'delta' => 'footer_mini_panel',
          'region' => 'footer_message',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('HD Client');
  $export['owner_sitewide'] = $context;

  return $export;
}
