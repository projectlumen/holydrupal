<?php
/**
 * @file
 * hd_music.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function hd_music_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  list($module, $api) = func_get_args();
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function hd_music_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function hd_music_node_info() {
  $items = array(
    'album' => array(
      'name' => t('Album'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('album name'),
      'help' => '',
    ),
    'artist' => array(
      'name' => t('Artist'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('artist name'),
      'help' => '',
    ),
    'band' => array(
      'name' => t('Band'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('band name'),
      'help' => '',
    ),
    'show' => array(
      'name' => t('Show'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('show name'),
      'help' => '',
    ),
    'track' => array(
      'name' => t('Track'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('track name'),
      'help' => '',
    ),
    'venue' => array(
      'name' => t('Venue'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('venue name'),
      'help' => '',
    ),
  );
  return $items;
}
