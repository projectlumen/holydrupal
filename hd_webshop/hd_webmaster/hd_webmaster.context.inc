<?php
/**
 * @file
 * hd_webmaster.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function hd_webmaster_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'webmaster_selective';
  $context->description = '';
  $context->tag = 'HD Webshop';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'account' => 'account',
        'control' => 'control',
        'domain' => 'domain',
        'email_address' => 'email_address',
        'platform' => 'platform',
        'site' => 'site',
        'site_theme' => 'site_theme',
        'template' => 'template',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
    'path' => array(
      'values' => array(
        'office/web' => 'office/web',
        'office/web/*' => 'office/web/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'menu-menu-webmaster' => array(
          'module' => 'menu',
          'delta' => 'menu-webmaster',
          'region' => 'menu_bar',
          'weight' => '-10',
        ),
      ),
    ),
    'menu' => 'office/web',
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('HD Webshop');
  $export['webmaster_selective'] = $context;

  return $export;
}
