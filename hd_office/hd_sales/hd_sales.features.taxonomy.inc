<?php
/**
 * @file
 * hd_sales.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function hd_sales_taxonomy_default_vocabularies() {
  return array(
    'company_position' => array(
      'name' => 'Company Position',
      'machine_name' => 'company_position',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
  );
}
