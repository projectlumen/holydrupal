<?php
/**
 * @file
 * hd_user_switch.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function hd_user_switch_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'user_switch';
  $context->description = '';
  $context->tag = 'HD Tools';
  $context->conditions = array(
    'user' => array(
      'values' => array(
        'authenticated user' => 'authenticated user',
      ),
    ),
  );
  $context->reactions = array();
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('HD Tools');
  $export['user_switch'] = $context;

  return $export;
}
