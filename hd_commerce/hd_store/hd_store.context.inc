<?php
/**
 * @file
 * hd_store.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function hd_store_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'store_blocks';
  $context->description = '';
  $context->tag = 'HD Store';
  $context->conditions = array(
    'context' => array(
      'values' => array(
        '~*_section' => '~*_section',
      ),
    ),
    'path' => array(
      'values' => array(
        '~cart' => '~cart',
        '~checkout' => '~checkout',
        '~checkout/*' => '~checkout/*',
        '~node/*' => '~node/*',
        '~user' => '~user',
        '~user/*' => '~user/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'commerce_cart-cart' => array(
          'module' => 'commerce_cart',
          'delta' => 'cart',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('HD Store');
  $export['store_blocks'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'store_progress';
  $context->description = '';
  $context->tag = 'HD Store';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'checkout' => 'checkout',
        'checkout/*' => 'checkout/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'commerce_checkout_progress-indication' => array(
          'module' => 'commerce_checkout_progress',
          'delta' => 'indication',
          'region' => 'highlighted',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('HD Store');
  $export['store_progress'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'store_sitewide';
  $context->description = '';
  $context->tag = 'HD Store';
  $context->conditions = array(
    'context' => array(
      'values' => array(
        '~*_section' => '~*_section',
      ),
    ),
    'path' => array(
      'values' => array(
        '~checkout' => '~checkout',
        '~checkout/*' => '~checkout/*',
      ),
    ),
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'system-powered-by' => array(
          'module' => 'system',
          'delta' => 'powered-by',
          'region' => 'footer',
          'weight' => '-10',
        ),
        'system-navigation' => array(
          'module' => 'system',
          'delta' => 'navigation',
          'region' => 'toolbar',
          'weight' => '-10',
        ),
        'system-main-menu' => array(
          'module' => 'system',
          'delta' => 'main-menu',
          'region' => 'menu_bar',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('HD Store');
  $export['store_sitewide'] = $context;

  return $export;
}
