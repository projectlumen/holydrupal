<?php
/**
 * @file
 * hd_sales.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function hd_sales_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'company_import';
  $feeds_importer->config = array(
    'name' => 'Company Import',
    'description' => 'Import company nodes from a CSV file',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => FALSE,
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'content_type' => 'company',
        'expire' => '-1',
        'author' => '0',
        'authorize' => 0,
        'mappings' => array(
          0 => array(
            'source' => 'Company Name',
            'target' => 'title',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'Parent Company',
            'target' => 'field_company_primary:title',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'Company Type',
            'target' => 'field_company_type',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'Company Status',
            'target' => 'field_company_status',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'Website',
            'target' => 'field_company_website:url',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'Phone',
            'target' => 'field_company_phone',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'Fax',
            'target' => 'field_company_fax',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'Email',
            'target' => 'field_company_email',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => 'Address',
            'target' => 'field_company_address',
            'unique' => FALSE,
          ),
          9 => array(
            'source' => 'City',
            'target' => 'field_company_city',
            'unique' => FALSE,
          ),
          10 => array(
            'source' => 'State',
            'target' => 'field_company_state',
            'unique' => FALSE,
          ),
          11 => array(
            'source' => 'Zip',
            'target' => 'field_company_zip',
            'unique' => FALSE,
          ),
          12 => array(
            'source' => 'Country',
            'target' => 'field_company_country',
            'unique' => FALSE,
          ),
          13 => array(
            'source' => 'Employees',
            'target' => 'field_company_employees',
            'unique' => FALSE,
          ),
          14 => array(
            'source' => 'Revenue',
            'target' => 'field_company_revenue',
            'unique' => FALSE,
          ),
          15 => array(
            'source' => 'Ownership',
            'target' => 'field_company_ownership',
            'unique' => FALSE,
          ),
          16 => array(
            'source' => 'Stock Ticker',
            'target' => 'field_company_ticker',
            'unique' => FALSE,
          ),
          17 => array(
            'source' => 'Jigsaw Link',
            'target' => 'field_company_jigsaw:url',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
      ),
    ),
    'content_type' => 'company_import',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['company_import'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'contact_import';
  $feeds_importer->config = array(
    'name' => 'Contact Import',
    'description' => 'Import contact nodes from a CSV file',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => FALSE,
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'content_type' => 'contact',
        'expire' => '-1',
        'author' => 0,
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'First Name',
            'target' => 'field_contact_first_name',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'Last Name',
            'target' => 'field_contact_last_name',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '0',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
      ),
    ),
    'content_type' => 'contact_import',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['contact_import'] = $feeds_importer;

  return $export;
}
