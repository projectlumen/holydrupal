<?php
/**
 * @file
 * hd_portal.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function hd_portal_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'portal_dashboard';
  $context->description = '';
  $context->tag = 'HD Portal';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'portal' => 'portal',
        'portal/dashboard' => 'portal/dashboard',
      ),
    ),
  );
  $context->reactions = array(
    'menu' => 'portal/dashboard',
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('HD Portal');
  $export['portal_dashboard'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'portal_orders';
  $context->description = '';
  $context->tag = 'HD Portal';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'portal/order' => 'portal/order',
        'portal/order/*' => 'portal/order/*',
        'portal/orders' => 'portal/orders',
        'portal/orders/*' => 'portal/orders/*',
      ),
    ),
  );
  $context->reactions = array(
    'breadcrumb' => 'portal/orders',
    'menu' => 'portal/orders',
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('HD Portal');
  $export['portal_orders'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'portal_section';
  $context->description = '';
  $context->tag = 'HD Portal';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'portal' => 'portal',
        'portal/*' => 'portal/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'menu-menu-portal-menu' => array(
          'module' => 'menu',
          'delta' => 'menu-portal-menu',
          'region' => 'menu_bar',
          'weight' => '-10',
        ),
      ),
    ),
    'menu' => 'portal',
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('HD Portal');
  $export['portal_section'] = $context;

  return $export;
}
