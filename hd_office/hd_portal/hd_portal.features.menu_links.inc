<?php
/**
 * @file
 * hd_portal.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function hd_portal_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-portal-menu:portal/dashboard
  $menu_links['menu-portal-menu:portal/dashboard'] = array(
    'menu_name' => 'menu-portal-menu',
    'link_path' => 'portal/dashboard',
    'router_path' => 'portal/dashboard',
    'link_title' => 'Dashboard',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -40,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Dashboard');


  return $menu_links;
}
