<?php
/**
 * @file
 * hd_webmaster.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function hd_webmaster_default_rules_configuration() {
  $items = array();
  $items['rules_control_update'] = entity_import('rules_config', '{ "rules_control_update" : {
      "LABEL" : "Control Update",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "HD Webshop" ],
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_presave", "node_view" ],
      "IF" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "control" : "control" } } } }
      ],
      "DO" : [ { "data_set" : { "data" : [ "node:status" ], "value" : "1" } } ]
    }
  }');
  $items['rules_domain'] = entity_import('rules_config', '{ "rules_domain" : {
      "LABEL" : "Domain",
      "PLUGIN" : "rule set",
      "TAGS" : [ "HD Webshop" ],
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "node" : { "label" : "Node", "type" : "node" } },
      "RULES" : [
        { "RULE" : {
            "IF" : [
              { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "site" : "site" } } } },
              { "data_is" : { "data" : [ "node:field-site-domain-option" ], "value" : "sub" } }
            ],
            "DO" : [
              { "data_set" : {
                  "data" : [ "node:field-site-url:url" ],
                  "value" : "http:\\/\\/[node:field_site_short_name].[node:field_site_root_domain]"
                }
              }
            ],
            "LABEL" : "Sub-domain"
          }
        },
        { "RULE" : {
            "IF" : [
              { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "site" : "site" } } } },
              { "data_is" : { "data" : [ "node:field-site-domain-option" ], "value" : "park" } }
            ],
            "DO" : [
              { "data_set" : {
                  "data" : [ "node:field-site-url:url" ],
                  "value" : "http:\\/\\/[node:field_site_root_domain]"
                }
              }
            ],
            "LABEL" : "Parked Domain"
          }
        }
      ]
    }
  }');
  $items['rules_domain_create'] = entity_import('rules_config', '{ "rules_domain_create" : {
      "LABEL" : "Domain Create",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "HD Webshop" ],
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_insert" ],
      "IF" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "domain" : "domain" } } } }
      ],
      "DO" : [
        { "component_rules_hd_shell" : {
            "USING" : { "command" : "cp addpark [node:title]" },
            "PROVIDE" : { "command" : { "command" : "Command" } }
          }
        }
      ]
    }
  }');
  $items['rules_domain_delete'] = entity_import('rules_config', '{ "rules_domain_delete" : {
      "LABEL" : "Domain Delete",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "HD Webshop" ],
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_delete" ],
      "IF" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "domain" : "domain" } } } }
      ],
      "DO" : [
        { "component_rules_hd_shell" : {
            "USING" : { "command" : "cp delpark [node:title]" },
            "PROVIDE" : { "command" : { "command" : "Command" } }
          }
        }
      ]
    }
  }');
  $items['rules_domain_update'] = entity_import('rules_config', '{ "rules_domain_update" : {
      "LABEL" : "Domain Update",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "HD Webshop" ],
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_update" ],
      "IF" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "domain" : "domain" } } } }
      ],
      "DO" : [
        { "data_set" : { "data" : [ "node:title" ], "value" : [ "node-unchanged:title" ] } }
      ]
    }
  }');
  $items['rules_email_create'] = entity_import('rules_config', '{ "rules_email_create" : {
      "LABEL" : "Email Create",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "HD Webshop" ],
      "REQUIRES" : [ "rules", "php" ],
      "ON" : [ "node_insert" ],
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "email_address" : "email_address" } }
          }
        }
      ],
      "DO" : [
        { "component_rules_email_create_rules" : { "node" : [ "node" ] } },
        { "LOOP" : {
            "USING" : { "list" : [ "node:field-email-forwarder" ] },
            "ITEM" : { "forwarder" : "Forwader" },
            "DO" : [
              { "component_rules_forwarder_create" : {
                  "user" : "[node:field_email_username]",
                  "root_domain" : "[node:field_email_root_domain]",
                  "forwarder" : "\\u003C?php return $forwarder ?\\u003E"
                }
              }
            ]
          }
        }
      ]
    }
  }');
  $items['rules_email_create_action'] = entity_import('rules_config', '{ "rules_email_create_action" : {
      "LABEL" : "Email Create Action",
      "PLUGIN" : "action set",
      "TAGS" : [ "HD Webshop" ],
      "REQUIRES" : [ "php", "rules" ],
      "USES VARIABLES" : {
        "node" : { "label" : "Node", "type" : "node" },
        "password" : { "label" : "Password", "type" : "text" }
      },
      "ACTION SET" : [
        { "component_rules_hd_shell" : {
            "USING" : { "command" : "\\u003C?php\\r\\n$cmd = \\u0022cp addmail\\u0022;\\r\\n$email_user = \\u0022[node:field_email_username]\\u0022;\\r\\n$root_domain = \\u0022[node:field_email_root_domain]\\u0022;\\r\\n$quota =  variable_get(\\u0022hd_webmaster_mailquota\\u0022, \\u0022250\\u0022);\\r\\n$execute = $cmd . \\u0027 \\u0027 . $email_user . \\u0027 \\u0027 . $root_domain . \\u0027 \\u0027 . $password . \\u0027 \\u0027 . $quota;\\r\\nreturn $execute;\\r\\n?\\u003E" },
            "PROVIDE" : { "command" : { "command" : "Command" } }
          }
        }
      ]
    }
  }');
  $items['rules_email_create_rules'] = entity_import('rules_config', '{ "rules_email_create_rules" : {
      "LABEL" : "Email Create Rules",
      "PLUGIN" : "rule set",
      "TAGS" : [ "HD Webshop" ],
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "node" : { "label" : "Node", "type" : "node" } },
      "RULES" : [
        { "RULE" : {
            "IF" : [
              { "node_is_of_type" : {
                  "node" : [ "node" ],
                  "type" : { "value" : { "email_address" : "email_address" } }
                }
              },
              { "data_is_empty" : { "data" : [ "node:field-email-password" ] } },
              { "entity_has_field" : { "entity" : [ "node:author" ], "field" : "field_profile_email_password" } }
            ],
            "DO" : [
              { "component_rules_email_create_action" : {
                  "node" : [ "node" ],
                  "password" : [ "node:author:field-profile-email-password" ]
                }
              }
            ],
            "LABEL" : "Password NOT present"
          }
        },
        { "RULE" : {
            "IF" : [
              { "node_is_of_type" : {
                  "node" : [ "node" ],
                  "type" : { "value" : { "email_address" : "email_address" } }
                }
              },
              { "NOT data_is_empty" : { "data" : [ "node:field-email-password" ] } }
            ],
            "DO" : [
              { "component_rules_email_create_action" : { "node" : [ "node" ], "password" : [ "node:field-email-password" ] } }
            ],
            "LABEL" : "Password IS present"
          }
        }
      ]
    }
  }');
  $items['rules_email_delete'] = entity_import('rules_config', '{ "rules_email_delete" : {
      "LABEL" : "Email Delete",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "HD Webshop" ],
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_delete" ],
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "email_address" : "email_address" } }
          }
        }
      ],
      "DO" : [
        { "LOOP" : {
            "USING" : { "list" : [ "node:field-email-forwarder" ] },
            "ITEM" : { "forwarder" : "Forwarder" },
            "DO" : [
              { "component_rules_forwarder_delete" : {
                  "user" : [ "node:field-email-username" ],
                  "root_domain" : [ "node:field-email-root-domain:title" ],
                  "forwarder" : [ "forwarder" ]
                }
              }
            ]
          }
        },
        { "component_rules_email_delete_action" : [] }
      ]
    }
  }');
  $items['rules_email_delete_action'] = entity_import('rules_config', '{ "rules_email_delete_action" : {
      "LABEL" : "Email Delete",
      "PLUGIN" : "action set",
      "TAGS" : [ "HD Webshop Email" ],
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "node" : { "label" : "Node", "type" : "node" } },
      "ACTION SET" : [
        { "component_rules_hd_shell" : {
            "USING" : { "command" : "cp delmail [node:field_email_username] [node:field_email_root_domain]" },
            "PROVIDE" : { "command" : { "command" : "Command" } }
          }
        }
      ]
    }
  }');
  $items['rules_email_update'] = entity_import('rules_config', '{ "rules_email_update" : {
      "LABEL" : "Email Update",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "HD Webshop" ],
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_update" ],
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "email_address" : "email_address" } }
          }
        }
      ],
      "DO" : [
        { "component_rules_email_update_rules" : { "old_node" : [ "node-unchanged" ], "new_node" : [ "node" ] } }
      ]
    }
  }');
  $items['rules_email_update_rules'] = entity_import('rules_config', '{ "rules_email_update_rules" : {
      "LABEL" : "Email Update Rules",
      "PLUGIN" : "rule set",
      "TAGS" : [ "HD Webshop Email" ],
      "REQUIRES" : [ "rules", "php" ],
      "USES VARIABLES" : {
        "old_node" : { "label" : "Old Node", "type" : "node" },
        "new_node" : { "label" : "New Node", "type" : "node" }
      },
      "RULES" : [
        { "RULE" : {
            "IF" : [
              { "node_is_of_type" : {
                  "node" : [ "old-node" ],
                  "type" : { "value" : { "email_address" : "email_address" } }
                }
              },
              { "node_is_of_type" : {
                  "node" : [ "new-node" ],
                  "type" : { "value" : { "email_address" : "email_address" } }
                }
              },
              { "OR" : [
                  { "NOT data_is" : {
                      "data" : [ "new-node:field-email-username" ],
                      "value" : [ "old-node:field-email-username" ]
                    }
                  },
                  { "NOT data_is" : {
                      "data" : [ "new-node:field-email-root-domain" ],
                      "value" : [ "old-node:field-email-root-domain" ]
                    }
                  }
                ]
              }
            ],
            "DO" : [
              { "LOOP" : {
                  "USING" : { "list" : [ "old-node:field-email-forwarder" ] },
                  "ITEM" : { "old_forwarder" : "Old Forwarder" },
                  "DO" : [
                    { "component_rules_forwarder_delete" : {
                        "user" : [ "old-node:field-email-username" ],
                        "root_domain" : [ "old-node:field-email-root-domain:title" ],
                        "forwarder" : [ "old-forwarder" ]
                      }
                    }
                  ]
                }
              },
              { "component_rules_email_delete_action" : { "node" : [ "old-node" ] } },
              { "component_rules_email_create_rules" : { "node" : [ "new-node" ] } },
              { "LOOP" : {
                  "USING" : { "list" : [ "new-node:field-email-forwarder" ] },
                  "ITEM" : { "new_forwarder" : "New Forwarder" },
                  "DO" : [
                    { "component_rules_forwarder_create" : {
                        "user" : [ "new-node:field-email-username" ],
                        "root_domain" : [ "new-node:field-email-root-domain:title" ],
                        "forwarder" : [ "new-forwarder" ]
                      }
                    }
                  ]
                }
              }
            ],
            "LABEL" : "User or Root have changed"
          }
        },
        { "RULE" : {
            "IF" : [
              { "node_is_of_type" : {
                  "node" : [ "old-node" ],
                  "type" : { "value" : { "email_address" : "email_address" } }
                }
              },
              { "node_is_of_type" : {
                  "node" : [ "new-node" ],
                  "type" : { "value" : { "email_address" : "email_address" } }
                }
              },
              { "data_is" : {
                  "data" : [ "new-node:field-email-username" ],
                  "value" : [ "old-node:field-email-username" ]
                }
              },
              { "data_is" : {
                  "data" : [ "new-node:field-email-root-domain:title" ],
                  "value" : [ "old-node:field-email-root-domain:title" ]
                }
              },
              { "NOT data_is" : {
                  "data" : [ "new-node:field-email-forwarder" ],
                  "value" : [ "old-node:field-email-forwarder" ]
                }
              }
            ],
            "DO" : [
              { "LOOP" : {
                  "USING" : { "list" : [ "old-node:field-email-forwarder" ] },
                  "ITEM" : { "old_forwarder" : "Old Forwarder" },
                  "DO" : [
                    { "component_rules_forwarder_delete" : {
                        "user" : [ "old-node:field-email-username" ],
                        "root_domain" : [ "old-node:field-email-root-domain:title" ],
                        "forwarder" : "\\u003C?php return $old_forwarder ?\\u003E"
                      }
                    }
                  ]
                }
              },
              { "LOOP" : {
                  "USING" : { "list" : [ "new-node:field-email-forwarder" ] },
                  "ITEM" : { "new_forwarder" : "New Forwarder" },
                  "DO" : [
                    { "component_rules_forwarder_create" : {
                        "user" : [ "new-node:field-email-username" ],
                        "root_domain" : [ "new-node:field-email-root-domain:title" ],
                        "forwarder" : "\\u003C?php return $new_forwarder ?\\u003E"
                      }
                    }
                  ]
                }
              }
            ],
            "LABEL" : "Forwarders have changed"
          }
        }
      ]
    }
  }');
  $items['rules_forwarder_create'] = entity_import('rules_config', '{ "rules_forwarder_create" : {
      "LABEL" : "Forwarder Create",
      "PLUGIN" : "action set",
      "TAGS" : [ "HD Webshop cPanel" ],
      "REQUIRES" : [ "php", "rules" ],
      "USES VARIABLES" : {
        "user" : { "label" : "User", "type" : "text" },
        "root_domain" : { "label" : "Root Domain", "type" : "text" },
        "forwarder" : { "label" : "Forwarding Email", "type" : "text" }
      },
      "ACTION SET" : [
        { "component_rules_hd_shell" : {
            "USING" : { "command" : "\\u003C?php\\r\\n$cmd = \\u0022cp addfwd\\u0022;\\r\\n$execute = $cmd . \\u0027 \\u0027 . $user . \\u0027 \\u0027 . $root_domain . \\u0027 \\u0027 . $forwarder;\\r\\nreturn $execute;\\r\\n?\\u003E" },
            "PROVIDE" : { "command" : { "command" : "Command" } }
          }
        }
      ]
    }
  }');
  $items['rules_forwarder_create_rules'] = entity_import('rules_config', '{ "rules_forwarder_create_rules" : {
      "LABEL" : "Forwarder Create Rules",
      "PLUGIN" : "rule set",
      "TAGS" : [ "HD Webshop" ],
      "REQUIRES" : [ "rules", "php" ],
      "USES VARIABLES" : { "node" : { "label" : "Node", "type" : "node" } },
      "RULES" : [
        { "RULE" : {
            "PROVIDE" : { "command" : { "command" : "Command" } },
            "IF" : [
              { "node_is_of_type" : {
                  "node" : [ "node" ],
                  "type" : { "value" : { "email_address" : "email_address" } }
                }
              },
              { "NOT data_is_empty" : { "data" : [ "node:field-email-forwarder" ] } }
            ],
            "DO" : [
              { "component_rules_hd_shell" : {
                  "USING" : { "command" : "\\u003C?php\\r\\n$cmd = \\u0022cp addmail\\u0022;\\r\\n$email_user = \\u0022[node:field_email_username]\\u0022;\\r\\n$root_domain = \\u0022[node:field_email_root_domain]\\u0022;\\r\\n$password = \\u0022[node:field_email_password]\\u0022;\\r\\n$quota =  variable_get(\\u0022hd_webmaster_mailquota\\u0022, \\u0022250\\u0022);\\r\\n$execute = $cmd . \\u0027 \\u0027 . $email_user . \\u0027 \\u0027 . $root_domain . \\u0027 \\u0027 . $password . \\u0027 \\u0027 . $quota;\\r\\nreturn $execute;\\r\\n?\\u003E" },
                  "PROVIDE" : { "command" : { "command" : "Command" } }
                }
              },
              { "LOOP" : {
                  "USING" : { "list" : [ "node:field-email-forwarder" ] },
                  "ITEM" : { "forwarder" : "Forwarder" },
                  "DO" : [
                    { "component_rules_forwarder_create" : {
                        "user" : [ "node:field-email-username" ],
                        "root_domain" : [ "node:field-email-root-domain:title" ],
                        "forwarder" : "\\u003C?php return $forwarder ?\\u003E"
                      }
                    }
                  ]
                }
              }
            ],
            "LABEL" : "Forwarder value is present"
          }
        },
        { "RULE" : {
            "IF" : [
              { "node_is_of_type" : {
                  "node" : [ "node" ],
                  "type" : { "value" : { "email_address" : "email_address" } }
                }
              },
              { "data_is_empty" : { "data" : [ "node:field-email-forwarder" ] } }
            ],
            "DO" : [
              { "component_rules_forwarder_create" : {
                  "user" : [ "node:field-email-username" ],
                  "root_domain" : [ "node:field-email-root-domain:title" ],
                  "forwarder" : "bimbo@gmail.com"
                }
              }
            ],
            "LABEL" : "Forwarder value NOT present"
          }
        }
      ]
    }
  }');
  $items['rules_forwarder_delete'] = entity_import('rules_config', '{ "rules_forwarder_delete" : {
      "LABEL" : "Forwarder Delete",
      "PLUGIN" : "action set",
      "TAGS" : [ "HD Webshop cPanel" ],
      "REQUIRES" : [ "php", "rules" ],
      "USES VARIABLES" : {
        "user" : { "label" : "User", "type" : "text" },
        "root_domain" : { "label" : "Root Domain", "type" : "text" },
        "forwarder" : { "label" : "Forwarding Email", "type" : "text" }
      },
      "ACTION SET" : [
        { "component_rules_hd_shell" : {
            "USING" : { "command" : "\\u003C?php\\r\\n$cmd = \\u0022cp delfwd\\u0022;\\r\\n$execute = $cmd . \\u0027 \\u0027 . $user . \\u0027 \\u0027 . $root_domain . \\u0027 \\u0027 . $forwarder;\\r\\nreturn $execute;\\r\\n?\\u003E" },
            "PROVIDE" : { "command" : { "command" : "Command" } }
          }
        }
      ]
    }
  }');
  $items['rules_link_condition_backup_server'] = entity_import('rules_config', '{ "rules_link_condition_backup_server" : {
      "LABEL" : "Rules link: Backup Server condition",
      "PLUGIN" : "and",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "node" : { "type" : "node", "label" : "node" } },
      "AND" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "control" : "control" } } } }
      ]
    }
  }');
  $items['rules_link_condition_commit_bash'] = entity_import('rules_config', '{ "rules_link_condition_commit_bash" : {
      "LABEL" : "Rules link: Commit Bash condition",
      "PLUGIN" : "and",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "node" : { "type" : "node", "label" : "node" } },
      "AND" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "control" : "control" } } } }
      ]
    }
  }');
  $items['rules_link_condition_commit_holydrupal'] = entity_import('rules_config', '{ "rules_link_condition_commit_holydrupal" : {
      "LABEL" : "Rules link: Commit Holy Drupal condition",
      "PLUGIN" : "and",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "node" : { "type" : "node", "label" : "node" } },
      "AND" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "control" : "control" } } } }
      ]
    }
  }');
  $items['rules_link_condition_install_site'] = entity_import('rules_config', '{ "rules_link_condition_install_site" : {
      "LABEL" : "Rules link: Install Site condition",
      "PLUGIN" : "and",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "node" : { "type" : "node", "label" : "node" } },
      "AND" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "site" : "site" } } } },
        { "data_is" : { "data" : [ "node:field-site-stage" ], "value" : "install" } }
      ]
    }
  }');
  $items['rules_link_condition_push_repos_to_bucket'] = entity_import('rules_config', '{ "rules_link_condition_push_repos_to_bucket" : {
      "LABEL" : "Rules link: Push Repos to Bucket condition",
      "PLUGIN" : "and",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "node" : { "label" : "node", "type" : "node" } },
      "AND" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "control" : "control" } } } }
      ]
    }
  }');
  $items['rules_link_condition_view_password'] = entity_import('rules_config', '{ "rules_link_condition_view_password" : {
      "LABEL" : "Rules link: View Password condition",
      "PLUGIN" : "and",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "node" : { "type" : "node", "label" : "node" } },
      "AND" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "account" : "account" } } } }
      ]
    }
  }');
  $items['rules_link_set_backup_server'] = entity_import('rules_config', '{ "rules_link_set_backup_server" : {
      "LABEL" : "Rules link: Backup Server rules set",
      "PLUGIN" : "rule set",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "node" : { "type" : "node", "label" : "node" } },
      "RULES" : [
        { "RULE" : {
            "PROVIDE" : { "command" : { "command" : "Command" } },
            "IF" : [
              { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "control" : "control" } } } }
            ],
            "DO" : [
              { "component_rules_hd_shell" : {
                  "USING" : { "command" : "ub back" },
                  "PROVIDE" : { "command" : { "command" : "Command" } }
                }
              },
              { "drupal_message" : { "message" : "You have backed up your server" } },
              { "data_set" : {
                  "data" : [ "node:field-control-latest-backup" ],
                  "value" : "[site:current-date]"
                }
              }
            ],
            "LABEL" : "Backup Server"
          }
        }
      ]
    }
  }');
  $items['rules_link_set_commit_bash'] = entity_import('rules_config', '{ "rules_link_set_commit_bash" : {
      "LABEL" : "Rules link: Commit Bash rules set",
      "PLUGIN" : "rule set",
      "REQUIRES" : [ "rules", "php" ],
      "USES VARIABLES" : { "node" : { "type" : "node", "label" : "node" } },
      "RULES" : [
        { "RULE" : {
            "PROVIDE" : { "command" : { "command" : "Command" } },
            "IF" : [
              { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "control" : "control" } } } }
            ],
            "DO" : [
              { "component_rules_hd_shell" : {
                  "USING" : { "command" : "\\u003C?php \\r\\n$comment = \\u0022\\\\\\u0022[node:field_control_comment]\\\\\\u0022\\u0022;\\r\\n$cmd = \\u0022ub bash \\u0022;\\r\\necho $cmd . $comment;\\r\\n?\\u003E" },
                  "PROVIDE" : { "command" : { "command" : "Command" } }
                }
              },
              { "drupal_message" : { "message" : "Successfully committed and pushed \\u003Ca href=\\u0022https:\\/\\/bitbucket.org\\/projectlumen\\/bash\\u0022 target=\\u0022_blank\\u0022\\u003EBash\\u003C\\/a\\u003E" } },
              { "data_set" : {
                  "data" : [ "node:field-control-bash-commit" ],
                  "value" : "[site:current-date]"
                }
              }
            ],
            "LABEL" : "Commit and Push Bash"
          }
        }
      ]
    }
  }');
  $items['rules_link_set_commit_holydrupal'] = entity_import('rules_config', '{ "rules_link_set_commit_holydrupal" : {
      "LABEL" : "Rules link: Commit Holy Drupal rules set",
      "PLUGIN" : "rule set",
      "REQUIRES" : [ "rules", "php" ],
      "USES VARIABLES" : { "node" : { "type" : "node", "label" : "node" } },
      "RULES" : [
        { "RULE" : {
            "PROVIDE" : { "command" : { "command" : "Command" } },
            "IF" : [
              { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "control" : "control" } } } }
            ],
            "DO" : [
              { "component_rules_hd_shell" : {
                  "USING" : { "command" : "\\u003C?php \\r\\n$comment = \\u0022\\\\\\u0022[node:field_control_comment]\\\\\\u0022\\u0022;\\r\\n$cmd = \\u0022ub holy \\u0022;\\r\\necho $cmd . $comment;\\r\\n?\\u003E" },
                  "PROVIDE" : { "command" : { "command" : "Command" } }
                }
              },
              { "drupal_message" : { "message" : "Successfully committed and pushed \\u003Ca href=\\u0022https:\\/\\/bitbucket.org\\/projectlumen\\/holydrupal\\u0022 target=\\u0022_blank\\u0022\\u003EHoly Drupal\\u003C\\/a\\u003E" } },
              { "data_set" : {
                  "data" : [ "node:field-control-hd-commit" ],
                  "value" : "[site:current-date]"
                }
              }
            ],
            "LABEL" : "Commit and Push Holy Drupal"
          }
        }
      ]
    }
  }');
  $items['rules_link_set_install_site'] = entity_import('rules_config', '{ "rules_link_set_install_site" : {
      "LABEL" : "Rules link: Install Site rules set",
      "PLUGIN" : "rule set",
      "REQUIRES" : [ "rules", "php" ],
      "USES VARIABLES" : { "node" : { "type" : "node", "label" : "node" } },
      "RULES" : [
        { "RULE" : {
            "PROVIDE" : { "command" : { "command" : "Command" } },
            "IF" : [
              { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "site" : "site" } } } }
            ],
            "DO" : [
              { "component_rules_hd_shell" : {
                  "USING" : { "command" : "\\u003C?php\\r\\n$cmd = \\u0022install\\u0022;\\r\\n$short_name = \\u0022[node:field-site-short-name]\\u0022;\\r\\n$profile = \\u0022[node:field_site_profile]\\u0022;\\r\\n$site_name = \\u0022\\\\\\u0022[node:title]\\\\\\u0022\\u0022;\\r\\n$site_theme = \\u0022[node:field_site_theme]\\u0022;\\r\\n\\r\\n$cmd_line = $cmd . \\u0022 \\u0022 . $short_name . \\u0022 \\u0022 . $profile . \\u0022 \\u0022 . $site_name . \\u0022 \\u0022 . $site_theme;\\r\\n\\r\\nreturn $cmd_line;\\r\\n?\\u003E" },
                  "PROVIDE" : { "command" : { "command" : "Command" } }
                }
              },
              { "data_set" : { "data" : [ "node:field-site-stage" ], "value" : "live" } },
              { "drupal_message" : { "message" : "\\u003C?php\\r\\n$cmd = \\u0022install\\u0022;\\r\\n$short_name = \\u0022[node:field-site-short-name]\\u0022;\\r\\n$profile = \\u0022[node:field_site_profile]\\u0022;\\r\\n$site_name = \\u0022\\\\\\u0022[node:title]\\\\\\u0022\\u0022;\\r\\n$site_theme = \\u0022[node:field_site_theme]\\u0022;\\r\\n\\r\\n$cmd_line = $cmd . \\u0022 \\u0022 . $short_name . \\u0022 \\u0022 . $profile . \\u0022 \\u0022 . $site_name . \\u0022 \\u0022 . $site_theme;\\r\\n\\r\\nreturn $cmd_line;\\r\\n?\\u003E" } }
            ],
            "LABEL" : "Install Site \\u0026 Change Stage to Live"
          }
        }
      ]
    }
  }');
  $items['rules_link_set_push_repos_to_bucket'] = entity_import('rules_config', '{ "rules_link_set_push_repos_to_bucket" : {
      "LABEL" : "Rules link: Push Repos to Bucket rules set",
      "PLUGIN" : "rule set",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "node" : { "label" : "node", "type" : "node" } },
      "RULES" : [
        { "RULE" : {
            "PROVIDE" : { "command" : { "command" : "Command" } },
            "IF" : [
              { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "control" : "control" } } } }
            ],
            "DO" : [
              { "component_rules_hd_shell" : {
                  "USING" : { "command" : "ub bucket" },
                  "PROVIDE" : { "command" : { "command" : "Command" } }
                }
              },
              { "data_set" : {
                  "data" : [ "node:field-control-bucket-update" ],
                  "value" : "[site:current-date]"
                }
              },
              { "drupal_message" : { "message" : "Successfully pushed all repositories to \\u003Ca href=\\u0022https:\\/\\/bitbucket.org\\/projectlumen\\/profile\\/repositories\\u0022 target=\\u0022_blank\\u0022\\u003EBitBucket.org\\u003C\\/a\\u003E" } }
            ],
            "LABEL" : "Push repos to bucket"
          }
        }
      ]
    }
  }');
  $items['rules_link_set_view_password'] = entity_import('rules_config', '{ "rules_link_set_view_password" : {
      "LABEL" : "Rules link: View Password rules set",
      "PLUGIN" : "rule set",
      "REQUIRES" : [ "rules", "php" ],
      "USES VARIABLES" : { "node" : { "type" : "node", "label" : "node" } },
      "RULES" : [
        { "RULE" : {
            "IF" : [
              { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "account" : "account" } } } }
            ],
            "DO" : [
              { "php_eval" : { "code" : "$langcode = $language[\\u0027language\\u0027];\\r\\n$lang = (array_key_exists($langcode, $field_data)) ? $langcode : \\u0027und\\u0027;\\r\\n\\r\\n$field_data = $node-\\u003Efield_password;\\r\\n$parts = array_values($field_data[$lang][0]);\\r\\n$password_value = password_field_decrypt($parts[0]);\\r\\n\\r\\ndrupal_set_message($password_value, \\u0027warning\\u0027);\\r\\n" } }
            ],
            "LABEL" : "De-crypt \\u0026 View Password"
          }
        }
      ]
    }
  }');
  $items['rules_parked_delete'] = entity_import('rules_config', '{ "rules_parked_delete" : {
      "LABEL" : "Parked Delete",
      "PLUGIN" : "rule set",
      "TAGS" : [ "HD Webshop" ],
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "node" : { "label" : "Node", "type" : "node" } },
      "RULES" : [
        { "RULE" : {
            "PROVIDE" : { "command" : { "command" : "Command" } },
            "IF" : [
              { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "site" : "site" } } } },
              { "data_is" : { "data" : [ "node:field-site-domain-option" ], "value" : "park" } }
            ],
            "DO" : [
              { "component_rules_hd_shell" : {
                  "USING" : { "command" : "cp delpark [node:field_site_root_domain]" },
                  "PROVIDE" : { "command" : { "command" : "Command" } }
                }
              },
              { "entity_delete" : { "data" : [ "node:field-site-root-domain" ] } }
            ],
            "LABEL" : "Delete Parked Domain"
          }
        }
      ]
    }
  }');
  $items['rules_platform_create'] = entity_import('rules_config', '{ "rules_platform_create" : {
      "LABEL" : "Platform Create",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "HD Webshop" ],
      "REQUIRES" : [ "rules", "php" ],
      "ON" : [ "node_insert" ],
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "platform" : "platform" } }
          }
        }
      ],
      "DO" : [
        { "variable_add" : {
            "USING" : { "type" : "text", "value" : "[node:field_platform_root]" },
            "PROVIDE" : { "variable_added" : { "platform_root" : "Platform Root" } }
          }
        },
        { "data_set" : {
            "data" : [ "node:field-platform-alias" ],
            "value" : "\\u003C?php return basename($platform_root); ?\\u003E"
          }
        },
        { "data_set" : {
            "data" : [ "node:title" ],
            "value" : "\\u003C?php \\r\\n$platform_alias = basename($platform_root);\\r\\n$platform_name = ucfirst($platform_alias);\\r\\nreturn $platform_name; \\r\\n?\\u003E"
          }
        },
        { "data_set" : { "data" : [ "node:status" ], "value" : "1" } },
        { "entity_save" : { "data" : [ "node" ], "immediate" : 1 } },
        { "component_rules_hd_shell" : {
            "USING" : { "command" : "addplat [node:field_platform_root]" },
            "PROVIDE" : { "command" : { "command" : "Command" } }
          }
        },
        { "data_set" : { "data" : [ "node:field-platform-status" ], "value" : "delete" } }
      ]
    }
  }');
  $items['rules_platform_delete'] = entity_import('rules_config', '{ "rules_platform_delete" : {
      "LABEL" : "Platform Delete",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "HD Webshop" ],
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_delete" ],
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "platform" : "platform" } }
          }
        }
      ],
      "DO" : [
        { "component_rules_hd_shell" : {
            "USING" : { "command" : "delplat [node:field_platform_alias]" },
            "PROVIDE" : { "command" : { "command" : "Command" } }
          }
        }
      ]
    }
  }');
  $items['rules_platform_update'] = entity_import('rules_config', '{ "rules_platform_update" : {
      "LABEL" : "Platform Update",
      "PLUGIN" : "reaction rule",
      "ACTIVE" : false,
      "TAGS" : [ "HD Webshop" ],
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_presave" ],
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "platform" : "platform" } }
          }
        }
      ],
      "DO" : [
        { "component_rules_platform_update_rules" : { "unchanged" : [ "node-unchanged" ], "saved" : [ "node" ] } }
      ]
    }
  }');
  $items['rules_platform_update_rules'] = entity_import('rules_config', '{ "rules_platform_update_rules" : {
      "LABEL" : "Platform Update Rules",
      "PLUGIN" : "rule set",
      "TAGS" : [ "HD Webshop" ],
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : {
        "unchanged" : { "label" : "Unchanged", "type" : "node" },
        "saved" : { "label" : "Saved", "type" : "node" }
      },
      "RULES" : [
        { "RULE" : {
            "IF" : [
              { "node_is_of_type" : {
                  "node" : [ "unchanged" ],
                  "type" : { "value" : { "platform" : "platform" } }
                }
              },
              { "NOT data_is" : { "data" : [ "unchanged:title" ], "value" : [ "saved:title" ] } },
              { "node_is_of_type" : {
                  "node" : [ "saved" ],
                  "type" : { "value" : { "platform" : "platform" } }
                }
              }
            ],
            "DO" : [
              { "data_set" : { "data" : [ "saved:title" ], "value" : [ "unchanged:title" ] } },
              { "drupal_message" : {
                  "message" : "You can not change the platform root directory once the platform has been created.",
                  "type" : "warning"
                }
              },
              { "data_set" : { "data" : [ "saved:status" ], "value" : "1" } }
            ],
            "LABEL" : "If changes"
          }
        }
      ]
    }
  }');
  $items['rules_site_create'] = entity_import('rules_config', '{ "rules_site_create" : {
      "LABEL" : "Site Create",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "HD Webshop" ],
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_insert" ],
      "IF" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "site" : "site" } } } },
        { "entity_has_field" : {
            "entity" : [ "node:field-site-platform" ],
            "field" : "field_platform_alias"
          }
        }
      ],
      "DO" : [
        { "component_rules_hd_shell" : {
            "USING" : { "command" : "build [node:field_site_short_name] [node:field_site_root_domain] live" },
            "PROVIDE" : { "command" : { "command" : "Command" } }
          }
        },
        { "component_rules_domain" : { "node" : [ "node" ] } },
        { "data_set" : { "data" : [ "node:field-site-stage" ], "value" : "install" } },
        { "drupal_message" : { "message" : [ "node:field-site-platform:field-platform-alias" ] } }
      ]
    }
  }');
  $items['rules_site_delete'] = entity_import('rules_config', '{ "rules_site_delete" : {
      "LABEL" : "Site Delete",
      "PLUGIN" : "reaction rule",
      "ACTIVE" : false,
      "TAGS" : [ "HD Webshop" ],
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_delete" ],
      "IF" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "site" : "site" } } } }
      ],
      "DO" : [
        { "component_rules_hd_shell" : {
            "USING" : { "command" : "music" },
            "PROVIDE" : { "command" : { "command" : "Command" } }
          }
        },
        { "component_rules_parked_delete" : { "node" : [ "node" ] } }
      ]
    }
  }');
  $items['rules_site_update'] = entity_import('rules_config', '{ "rules_site_update" : {
      "LABEL" : "Site Update",
      "PLUGIN" : "reaction rule",
      "ACTIVE" : false,
      "TAGS" : [ "HD Webshop" ],
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_update" ],
      "IF" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "site" : "site" } } } }
      ],
      "DO" : [
        { "data_set" : {
            "data" : [ "node:field-site-root-domain" ],
            "value" : [ "node-unchanged:field-site-root-domain" ]
          }
        },
        { "data_set" : {
            "data" : [ "node:field-site-short-name" ],
            "value" : [ "node-unchanged:field-site-short-name" ]
          }
        },
        { "data_set" : {
            "data" : [ "node:field-site-domain-option" ],
            "value" : [ "node-unchanged:field-site-domain-option" ]
          }
        },
        { "data_set" : {
            "data" : [ "node:field-site-profile" ],
            "value" : [ "node-unchanged:field-site-profile" ]
          }
        },
        { "component_rules_hd_shell" : {
            "USING" : { "command" : "edit [node-unchanged:field_site_short_name] [node-unchanged:field_site_root_domain] [node-unchanged:field_site_type] [node-unchanged:field_site_domain_option] \\\\\\u0022[node:title]\\\\\\u0022 [node:field_site_theme]" },
            "PROVIDE" : { "command" : { "command" : "Command" } }
          }
        }
      ]
    }
  }');
  return $items;
}
