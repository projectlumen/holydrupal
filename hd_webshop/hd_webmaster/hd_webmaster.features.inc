<?php
/**
 * @file
 * hd_webmaster.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function hd_webmaster_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  list($module, $api) = func_get_args();
  if ($module == "corresponding_node_references" && $api == "default_corresponding_node_references_presets") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "rel" && $api == "rel") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function hd_webmaster_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function hd_webmaster_node_info() {
  $items = array(
    'control' => array(
      'name' => t('Control'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Controller'),
      'help' => '',
    ),
    'domain' => array(
      'name' => t('Domain'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Domain Name'),
      'help' => '',
    ),
    'email_address' => array(
      'name' => t('Email Address'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Email Address'),
      'help' => '',
    ),
    'platform' => array(
      'name' => t('Platform'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Platform Name'),
      'help' => '',
    ),
    'site' => array(
      'name' => t('Site'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Site Name'),
      'help' => '',
    ),
    'site_theme' => array(
      'name' => t('Theme'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
    'template' => array(
      'name' => t('Template'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Implements hook_default_rules_link().
 */
function hd_webmaster_default_rules_link() {
  $items = array();
  $items['backup_server'] = entity_import('rules_link', '{
    "settings" : {
      "text" : "Backup Server",
      "link_type" : "token",
      "bundles" : { "control" : "control" },
      "entity_link" : 1
    },
    "name" : "backup_server",
    "label" : "Backup Server",
    "path" : "backup-server",
    "entity_type" : "node"
  }');
  $items['commit_bash'] = entity_import('rules_link', '{
    "settings" : {
      "text" : "Commit and push Bash",
      "link_type" : "token",
      "bundles" : { "control" : "control" },
      "entity_link" : 1
    },
    "name" : "commit_bash",
    "label" : "Commit Bash",
    "path" : "commit-bash",
    "entity_type" : "node"
  }');
  $items['commit_holydrupal'] = entity_import('rules_link', '{
    "settings" : {
      "text" : "Commit and push Holy Drupal",
      "link_type" : "token",
      "bundles" : { "control" : "control" },
      "entity_link" : 1
    },
    "name" : "commit_holydrupal",
    "label" : "Commit Holy Drupal",
    "path" : "commit-hd",
    "entity_type" : "node"
  }');
  $items['install_site'] = entity_import('rules_link', '{
    "settings" : {
      "text" : "Install Site",
      "link_type" : "confirm",
      "bundles" : { "site" : "site" },
      "entity_link" : 1,
      "confirm_question" : "Are you sure you want to install this site?",
      "confirm_description" : ""
    },
    "name" : "install_site",
    "label" : "Install Site",
    "path" : "install",
    "entity_type" : "node"
  }');
  $items['push_repos_to_bucket'] = entity_import('rules_link', '{
    "settings" : {
      "text" : "Push repos to bucket",
      "link_type" : "token",
      "bundles" : { "control" : "control" },
      "entity_link" : 1
    },
    "name" : "push_repos_to_bucket",
    "label" : "Push Repos to Bucket",
    "path" : "push-bucket",
    "entity_type" : "node"
  }');
  $items['view_password'] = entity_import('rules_link', '{
    "settings" : {
      "text" : "View Password",
      "link_type" : "token",
      "bundles" : { "account" : "account" },
      "entity_link" : 1,
      "confirm_question" : "Are you sure you want to view this password?",
      "confirm_description" : ""
    },
    "name" : "view_password",
    "label" : "View Password",
    "path" : "view-password",
    "entity_type" : "node"
  }');
  return $items;
}
