<?php
/**
 * @file
 * hd_webportal.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function hd_webportal_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: page_manager_pages
  $overrides["page_manager_pages.portal.default_handlers|page_portal_panel_context|conf|display|content|new-1|configuration|arguments"]["DELETED"] = TRUE;
  $overrides["page_manager_pages.portal.default_handlers|page_portal_panel_context|conf|display|content|new-1|configuration|context"]["DELETED"] = TRUE;
  $overrides["page_manager_pages.portal.default_handlers|page_portal_panel_context|conf|display|content|new-1|subtype"] = 'portal_websites-dash_pane';
  $overrides["page_manager_pages.portal.default_handlers|page_portal_panel_context|conf|display|content|new-2|panel"] = 'left';
  $overrides["page_manager_pages.portal.default_handlers|page_portal_panel_context|conf|display|content|new-2|position"] = 1;
  $overrides["page_manager_pages.portal.default_handlers|page_portal_panel_context|conf|display|content|new-2|subtype"] = 'portal_domains-dash_pane';
  $overrides["page_manager_pages.portal.default_handlers|page_portal_panel_context|conf|display|content|new-3"] = (object) array(
      'pid' => 'new-3',
      'panel' => 'left',
      'type' => 'views_panes',
      'subtype' => 'portal_emails-dash_pane',
      'shown' => TRUE,
      'access' => array(),
      'configuration' => array(),
      'cache' => array(),
      'style' => array(
        'settings' => NULL,
      ),
      'css' => array(),
      'extras' => array(),
      'position' => 2,
      'locks' => array(),
    );
  $overrides["page_manager_pages.portal.default_handlers|page_portal_panel_context|conf|display|content|new-4"] = (object) array(
      'pid' => 'new-4',
      'panel' => 'right',
      'type' => 'views_panes',
      'subtype' => 'portal_user-panel_pane_1',
      'shown' => TRUE,
      'access' => array(),
      'configuration' => array(
        'arguments' => array(
          'uid' => '%user:uid',
        ),
        'context' => array(
          0 => 'context_user_1',
        ),
      ),
      'cache' => array(),
      'style' => array(
        'settings' => NULL,
      ),
      'css' => array(),
      'extras' => array(),
      'position' => 0,
      'locks' => array(),
    );
  $overrides["page_manager_pages.portal.default_handlers|page_portal_panel_context|conf|display|content|new-5"] = (object) array(
      'pid' => 'new-5',
      'panel' => 'right',
      'type' => 'views_panes',
      'subtype' => 'portal_orders-orders_pane',
      'shown' => TRUE,
      'access' => array(),
      'configuration' => array(),
      'cache' => array(),
      'style' => array(
        'settings' => NULL,
      ),
      'css' => array(),
      'extras' => array(),
      'position' => 1,
      'locks' => array(),
    );
  $overrides["page_manager_pages.portal.default_handlers|page_portal_panel_context|conf|display|panels|left|1"] = 'new-2';
  $overrides["page_manager_pages.portal.default_handlers|page_portal_panel_context|conf|display|panels|left|2"] = 'new-3';
  $overrides["page_manager_pages.portal.default_handlers|page_portal_panel_context|conf|display|panels|right|0"] = 'new-4';
  $overrides["page_manager_pages.portal.default_handlers|page_portal_panel_context|conf|display|panels|right|1"] = 'new-5';

 return $overrides;
}
