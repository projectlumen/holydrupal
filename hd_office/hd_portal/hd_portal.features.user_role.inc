<?php
/**
 * @file
 * hd_portal.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function hd_portal_user_default_roles() {
  $roles = array();

  // Exported role: client.
  $roles['client'] = array(
    'name' => 'client',
    'weight' => 3,
  );

  // Exported role: contributor.
  $roles['contributor'] = array(
    'name' => 'contributor',
    'weight' => 4,
  );

  // Exported role: manager.
  $roles['manager'] = array(
    'name' => 'manager',
    'weight' => 5,
  );

  return $roles;
}
