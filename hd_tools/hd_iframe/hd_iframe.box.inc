<?php
/**
 * @file
 * hd_iframe.box.inc
 */

/**
 * Implements hook_default_box().
 */
function hd_iframe_default_box() {
  $export = array();

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'freshbooks';
  $box->plugin_key = 'simple';
  $box->title = 'Portal';
  $box->description = 'Freshbooks iFrame';
  $box->options = array(
    'body' => array(
      'value' => '<iframe src="https://projectlumen.freshbooks.com" name="Portal" width="806" marginwidth="0" height="1200" marginheight="5" align="middle" scrolling="No" seamless></iframe>',
      'format' => 'php_code',
    ),
    'additional_classes' => '',
  );
  $export['freshbooks'] = $box;

  return $export;
}
