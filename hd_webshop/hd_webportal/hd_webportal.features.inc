<?php
/**
 * @file
 * hd_webportal.features.inc
 */

/**
 * Implements hook_views_api().
 */
function hd_webportal_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_default_page_manager_pages_alter().
 */
function hd_webportal_default_page_manager_pages_alter(&$data) {
  if (isset($data['portal'])) {
    $data['portal']->default_handlers['page_portal_panel_context']->conf['display']->content['new-1']->subtype = 'portal_websites-dash_pane'; /* WAS: 'portal_user-panel_pane_1' */
    $data['portal']->default_handlers['page_portal_panel_context']->conf['display']->content['new-2']->panel = 'left'; /* WAS: 'right' */
    $data['portal']->default_handlers['page_portal_panel_context']->conf['display']->content['new-2']->position = 1; /* WAS: 0 */
    $data['portal']->default_handlers['page_portal_panel_context']->conf['display']->content['new-2']->subtype = 'portal_domains-dash_pane'; /* WAS: 'portal_orders-orders_pane' */
    $data['portal']->default_handlers['page_portal_panel_context']->conf['display']->content['new-3'] = (object) array(
          'pid' => 'new-3',
          'panel' => 'left',
          'type' => 'views_panes',
          'subtype' => 'portal_emails-dash_pane',
          'shown' => TRUE,
          'access' => array(),
          'configuration' => array(),
          'cache' => array(),
          'style' => array(
            'settings' => NULL,
          ),
          'css' => array(),
          'extras' => array(),
          'position' => 2,
          'locks' => array(),
        ); /* WAS: '' */
    $data['portal']->default_handlers['page_portal_panel_context']->conf['display']->content['new-4'] = (object) array(
          'pid' => 'new-4',
          'panel' => 'right',
          'type' => 'views_panes',
          'subtype' => 'portal_user-panel_pane_1',
          'shown' => TRUE,
          'access' => array(),
          'configuration' => array(
            'arguments' => array(
              'uid' => '%user:uid',
            ),
            'context' => array(
              0 => 'context_user_1',
            ),
          ),
          'cache' => array(),
          'style' => array(
            'settings' => NULL,
          ),
          'css' => array(),
          'extras' => array(),
          'position' => 0,
          'locks' => array(),
        ); /* WAS: '' */
    $data['portal']->default_handlers['page_portal_panel_context']->conf['display']->content['new-5'] = (object) array(
          'pid' => 'new-5',
          'panel' => 'right',
          'type' => 'views_panes',
          'subtype' => 'portal_orders-orders_pane',
          'shown' => TRUE,
          'access' => array(),
          'configuration' => array(),
          'cache' => array(),
          'style' => array(
            'settings' => NULL,
          ),
          'css' => array(),
          'extras' => array(),
          'position' => 1,
          'locks' => array(),
        ); /* WAS: '' */
    $data['portal']->default_handlers['page_portal_panel_context']->conf['display']->panels['left']['1'] = 'new-2'; /* WAS: '' */
    $data['portal']->default_handlers['page_portal_panel_context']->conf['display']->panels['left']['2'] = 'new-3'; /* WAS: '' */
    $data['portal']->default_handlers['page_portal_panel_context']->conf['display']->panels['right']['0'] = 'new-4'; /* WAS: 'new-2' */
    $data['portal']->default_handlers['page_portal_panel_context']->conf['display']->panels['right']['1'] = 'new-5'; /* WAS: '' */
    unset($data['portal']->default_handlers['page_portal_panel_context']->conf['display']->content['new-1']->configuration['arguments']);
    unset($data['portal']->default_handlers['page_portal_panel_context']->conf['display']->content['new-1']->configuration['context']);
  }
}
