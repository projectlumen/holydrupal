<?php
/**
 * @file
 * hd_store.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function hd_store_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_product_display';
  $strongarm->value = 0;
  $export['comment_anonymous_product_display'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_product_display';
  $strongarm->value = 1;
  $export['comment_default_mode_product_display'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_product_display';
  $strongarm->value = '50';
  $export['comment_default_per_page_product_display'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_product_display';
  $strongarm->value = 1;
  $export['comment_form_location_product_display'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_product_display';
  $strongarm->value = '1';
  $export['comment_preview_product_display'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_product_display';
  $strongarm->value = '1';
  $export['comment_product_display'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_product_display';
  $strongarm->value = 1;
  $export['comment_subject_field_product_display'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'commerce_checkout_progress_cart';
  $strongarm->value = 1;
  $export['commerce_checkout_progress_cart'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'commerce_checkout_progress_link';
  $strongarm->value = 1;
  $export['commerce_checkout_progress_link'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'commerce_checkout_progress_list_type';
  $strongarm->value = 'ol';
  $export['commerce_checkout_progress_list_type'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'commerce_customer_profile_billing_field';
  $strongarm->value = 'commerce_customer_billing';
  $export['commerce_customer_profile_billing_field'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'commerce_order_account_pane_auth_display';
  $strongarm->value = 1;
  $export['commerce_order_account_pane_auth_display'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'commerce_order_account_pane_mail_double_entry';
  $strongarm->value = 0;
  $export['commerce_order_account_pane_mail_double_entry'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__product_display';
  $strongarm->value = array(
    'view_modes' => array(
      'teaser' => array(
        'custom_settings' => TRUE,
      ),
      'full' => array(
        'custom_settings' => FALSE,
      ),
      'rss' => array(
        'custom_settings' => FALSE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
      'revision' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(
        'title' => array(
          'weight' => '-5',
        ),
        'path' => array(
          'weight' => '30',
        ),
      ),
      'display' => array(
        'product:sku' => array(
          'default' => array(
            'weight' => '8',
            'visible' => FALSE,
          ),
          'teaser' => array(
            'weight' => '4',
            'visible' => FALSE,
          ),
        ),
        'product:title' => array(
          'default' => array(
            'weight' => '9',
            'visible' => FALSE,
          ),
          'teaser' => array(
            'weight' => '5',
            'visible' => FALSE,
          ),
        ),
        'product:status' => array(
          'default' => array(
            'weight' => '10',
            'visible' => FALSE,
          ),
          'teaser' => array(
            'weight' => '7',
            'visible' => FALSE,
          ),
        ),
        'product:commerce_price' => array(
          'default' => array(
            'weight' => '2',
            'visible' => TRUE,
          ),
          'teaser' => array(
            'weight' => '2',
            'visible' => TRUE,
          ),
        ),
        'product:field_commerce_sp_time' => array(
          'teaser' => array(
            'weight' => '2',
            'visible' => TRUE,
          ),
          'default' => array(
            'weight' => '5',
            'visible' => TRUE,
          ),
        ),
        'product:field_commerce_sp_paypal' => array(
          'teaser' => array(
            'weight' => '5',
            'visible' => TRUE,
          ),
          'default' => array(
            'weight' => '6',
            'visible' => TRUE,
          ),
        ),
        'product:field_domain_url' => array(
          'teaser' => array(
            'weight' => '36',
            'visible' => TRUE,
          ),
          'default' => array(
            'weight' => '7',
            'visible' => TRUE,
          ),
        ),
        'product:field_product_image' => array(
          'default' => array(
            'weight' => '0',
            'visible' => TRUE,
          ),
          'teaser' => array(
            'weight' => '0',
            'visible' => TRUE,
          ),
        ),
      ),
    ),
  );
  $export['field_bundle_settings_node__product_display'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_main_links_source';
  $strongarm->value = '';
  $export['menu_main_links_source'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_product_display';
  $strongarm->value = array();
  $export['menu_options_product_display'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_product_display';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_product_display'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_product_display';
  $strongarm->value = array(
    0 => 'status',
  );
  $export['node_options_product_display'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_product_display';
  $strongarm->value = '0';
  $export['node_preview_product_display'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_product_display';
  $strongarm->value = 0;
  $export['node_submitted_product_display'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'theme_andromeda_settings';
  $strongarm->value = array(
    'toggle_logo' => 1,
    'toggle_name' => 1,
    'toggle_slogan' => 0,
    'toggle_node_user_picture' => 1,
    'toggle_comment_user_picture' => 1,
    'toggle_comment_user_verification' => 1,
    'toggle_favicon' => 1,
    'toggle_main_menu' => 1,
    'toggle_secondary_menu' => 1,
    'default_logo' => 1,
    'logo_path' => '',
    'logo_upload' => '',
    'default_favicon' => 1,
    'favicon_path' => '',
    'favicon_upload' => '',
    'andromeda_theme_settings__active_tab' => 'edit-andromeda-theme-options',
    'andromeda_enable_logo' => 1,
    'andromeda_logo_position' => 'left',
    'andromeda_sidebar_visibility' => '',
    'andromeda_show_front_page_title' => 0,
  );
  $export['theme_andromeda_settings'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'theme_business_settings';
  $strongarm->value = array(
    'toggle_logo' => 1,
    'toggle_name' => 1,
    'toggle_slogan' => 1,
    'toggle_node_user_picture' => 1,
    'toggle_comment_user_picture' => 1,
    'toggle_comment_user_verification' => 1,
    'toggle_favicon' => 1,
    'toggle_main_menu' => 1,
    'toggle_secondary_menu' => 1,
    'default_logo' => 1,
    'logo_path' => '',
    'logo_upload' => '',
    'default_favicon' => 1,
    'favicon_path' => '',
    'favicon_upload' => '',
    'show_front_content' => 1,
    'breadcrumbs' => 1,
    'slideshow_display' => 1,
    'slide1_desc' => 'Our Products',
    'slide1_url' => 'catalog',
    'slide2_desc' => 'Who We Are',
    'slide2_url' => 'node/2',
    'slide3_desc' => 'Our Service',
    'slide3_url' => 'node/3',
    'footer_copyright' => 1,
    'footer_credits' => 1,
  );
  $export['theme_business_settings'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'theme_storefront_settings';
  $strongarm->value = array(
    'toggle_logo' => 1,
    'toggle_name' => 1,
    'toggle_slogan' => 1,
    'toggle_node_user_picture' => 1,
    'toggle_comment_user_picture' => 1,
    'toggle_comment_user_verification' => 1,
    'toggle_favicon' => 1,
    'toggle_main_menu' => 1,
    'toggle_secondary_menu' => 1,
    'default_logo' => 1,
    'logo_path' => '',
    'logo_upload' => '',
    'default_favicon' => 1,
    'favicon_path' => '',
    'favicon_upload' => '',
    'dlayout_layout' => 'three-col-right',
    'dlayout_framing' => 'framed',
    'dlayout_page_unit' => '%',
    'dlayout_page_width' => '90',
    'dlayout_set_max_width' => 1,
    'dlayout_max_width_unit' => 'px',
    'dlayout_max_width' => '1260',
    'grid_columns' => 'container-24',
    'grid_24_first' => '6',
    'grid_24_second' => '6',
    'grid_16_first' => '4',
    'grid_16_second' => '4',
    'grid_12_first' => '3',
    'grid_12_second' => '3',
    'widescreen' => 1,
    'tablet' => 1,
    'smartphone' => 1,
    'mobilemenu' => 'slide',
    'breadcrumb_display' => 'yes',
    'breadcrumb_separator' => ' &#187; ',
    'breadcrumb_home' => 1,
    'content_display_grids_frontpage' => 0,
    'content_display_grids_frontpage_colcount' => '2',
    'content_display_grids_taxonomy_pages' => 1,
    'content_display_grids_taxonomy_pages_colcount' => 'tpcc-3',
    'menu_item_span_elements' => 0,
    'node_image_float' => 'image-left',
    'sf__active_tab' => 'edit-dlayout',
    'scheme' => 'scheme_dark',
    'palette' => array(
      'body' => '#666666',
      'headertop' => '#414042',
      'headerbottom' => '#000000',
      'headertext' => '#2aace2',
      'navbartop' => '#414042',
      'navbarbottom' => '#000000',
      'navbartext' => '#efefef',
      'navbarhover' => '#187da7',
      'navbarhover2' => '#44c8f5',
      'navbarhovertext' => '#ffffff',
      'secondary' => '#414042',
      'secondarytext' => '#ffffff',
      'secondaryhover' => '#2AACE2',
      'mainbackground' => '#c7c7c7',
      'maintext' => '#111111',
      'links' => '#1e5b92',
      'linkshover' => '#64a7d5',
      'atctop' => '#ffff00',
      'atcbottom' => '#ffcc00',
      'footer' => '#292929',
      'footertext' => '#e7e7e7',
      'footerlinks' => '#1e5b93',
      'footerhover' => '#64a7d6',
    ),
    'theme' => 'storefront',
    'info' => array(
      'fields' => array(
        'body' => 'Body Background',
        'headertop' => 'Header Top',
        'headerbottom' => 'Header Bottom',
        'headertext' => 'Header Text',
        'navbartop' => 'Navbar Top',
        'navbarbottom' => 'Navbar Bottom',
        'navbartext' => 'Navbar Text',
        'navbarhover' => 'Navbar Hover',
        'navbarhover2' => 'Navbar Hover Shade',
        'navbarhovertext' => 'Navbar Hover Text',
        'secondary' => 'Secondary Bar',
        'secondarytext' => 'Secondary Text',
        'secondaryhover' => 'Secondary Hover',
        'mainbackground' => 'Content Background',
        'maintext' => 'Content Text',
        'links' => 'Links',
        'linkshover' => 'Links Hover',
        'atctop' => 'Add To Cart Top',
        'atcbottom' => 'Add To Cart Bottom',
        'footer' => 'Footer Background',
        'footertext' => 'Footer Text',
        'footerlinks' => 'Footer Links',
        'footerhover' => 'Footer Hover',
      ),
      'schemes' => array(
        'default' => array(
          'title' => 'Drupal Commerce (default)',
          'colors' => array(
            'body' => '#cccccc',
            'headertop' => '#414042',
            'headerbottom' => '#414043',
            'headertext' => '#ffffff',
            'navbartop' => '#414044',
            'navbarbottom' => '#000000',
            'navbartext' => '#efefef',
            'navbarhover' => '#2aace2',
            'navbarhover2' => '#44c8f5',
            'navbarhovertext' => '#333333',
            'secondary' => '#6d6e71',
            'secondarytext' => '#fcfcfc',
            'secondaryhover' => '#2aace3',
            'mainbackground' => '#eeeeee',
            'maintext' => '#414045',
            'links' => '#2aace4',
            'linkshover' => '#3366cc',
            'atctop' => '#ffff00',
            'atcbottom' => '#ffcc00',
            'footer' => '#414041',
            'footertext' => '#e7e7e7',
            'footerlinks' => '#2aace5',
            'footerhover' => '#3366cd',
          ),
        ),
        'scheme_dark' => array(
          'title' => 'Drupal Commerce Dark',
          'colors' => array(
            'body' => '#666666',
            'headertop' => '#414042',
            'headerbottom' => '#000000',
            'headertext' => '#2aace2',
            'navbartop' => '#414042',
            'navbarbottom' => '#000000',
            'navbartext' => '#efefef',
            'navbarhover' => '#187da7',
            'navbarhover2' => '#44c8f5',
            'navbarhovertext' => '#ffffff',
            'secondary' => '#414042',
            'secondarytext' => '#ffffff',
            'secondaryhover' => '#2AACE2',
            'mainbackground' => '#c7c7c7',
            'maintext' => '#111111',
            'links' => '#1e5b92',
            'linkshover' => '#64a7d5',
            'atctop' => '#ffff00',
            'atcbottom' => '#ffcc00',
            'footer' => '#292929',
            'footertext' => '#e7e7e7',
            'footerlinks' => '#1e5b93',
            'footerhover' => '#64a7d6',
          ),
        ),
        'scheme_light' => array(
          'title' => 'Drupal Commerce Light',
          'colors' => array(
            'body' => '#eeeeee',
            'headertop' => '#ffffff',
            'headerbottom' => '#eeeeee',
            'headertext' => '#414042',
            'navbartop' => '#eeeeee',
            'navbarbottom' => '#cccccc',
            'navbartext' => '#333333',
            'navbarhover' => '#2aace2',
            'navbarhover2' => '#44c8f5',
            'navbarhovertext' => '#ffffff',
            'secondary' => '#eeeeee',
            'secondarytext' => '#333333',
            'secondaryhover' => '#2aace2',
            'mainbackground' => '#ffffff',
            'maintext' => '#414042',
            'links' => '#2AACE2',
            'linkshover' => '#44c8f5',
            'atctop' => '#ffff00',
            'atcbottom' => '#ffcc00',
            'footer' => '#eeeeee',
            'footertext' => '#333333',
            'footerlinks' => '#2AACE2',
            'footerhover' => '#44c8f5',
          ),
        ),
        'scheme_slate' => array(
          'title' => 'Drupal Slate',
          'colors' => array(
            'body' => '#292929',
            'headertop' => '#64a7d4',
            'headerbottom' => '#1e5b91',
            'headertext' => '#ffffff',
            'navbartop' => '#eeeeee',
            'navbarbottom' => '#cccccc',
            'navbartext' => '#333333',
            'navbarhover' => '#bcd6f0',
            'navbarhover2' => '#bcd6f0',
            'navbarhovertext' => '#333333',
            'secondary' => '#c7c7c7',
            'secondarytext' => '#ffffff',
            'secondaryhover' => '#64a7d4',
            'mainbackground' => '#f5f5f5',
            'maintext' => '#111111',
            'links' => '#1e5b92',
            'linkshover' => '#64a7d5',
            'atctop' => '#ffff00',
            'atcbottom' => '#ffcc00',
            'footer' => '#292929',
            'footertext' => '#e7e7e7',
            'footerlinks' => '#1e5b93',
            'footerhover' => '#64a7d6',
          ),
        ),
        '' => array(
          'title' => 'Custom',
          'colors' => array(),
        ),
      ),
      'css' => array(
        0 => 'css/colors.css',
      ),
      'copy' => array(
        0 => 'logo.png',
      ),
      'gradients' => array(
        0 => array(
          'dimension' => array(
            0 => 0,
            1 => 0,
            2 => 0,
            3 => 0,
          ),
          'direction' => 'vertical',
          'colors' => array(
            0 => 'top',
            1 => 'bottom',
          ),
        ),
      ),
      'fill' => array(),
      'slices' => array(),
      'blend_target' => '#ffffff',
      'preview_image' => 'color/color-preview.png',
      'preview_css' => 'color/preview.css',
      'base_image' => 'color/base.png',
    ),
  );
  $export['theme_storefront_settings'] = $strongarm;

  return $export;
}
