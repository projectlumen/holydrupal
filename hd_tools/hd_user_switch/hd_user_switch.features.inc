<?php
/**
 * @file
 * hd_user_switch.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function hd_user_switch_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
}
