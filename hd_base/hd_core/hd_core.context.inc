<?php
/**
 * @file
 * hd_core.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function hd_core_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'core_sitewide';
  $context->description = 'Sitewide contexts throughout Holy Drupal';
  $context->tag = 'HD Core';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'system-user-menu' => array(
          'module' => 'system',
          'delta' => 'user-menu',
          'region' => 'top_menu',
          'weight' => '-10',
        ),
        'system-main' => array(
          'module' => 'system',
          'delta' => 'main',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('HD Core');
  t('Sitewide contexts throughout Holy Drupal');
  $export['core_sitewide'] = $context;

  return $export;
}
