<?php
/**
 * @file
 * hd_music.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function hd_music_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'music_selective';
  $context->description = '';
  $context->tag = 'HD Music';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'album' => 'album',
        'artist' => 'artist',
        'band' => 'band',
        'show' => 'show',
        'track' => 'track',
        'venue' => 'venue',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
    'path' => array(
      'values' => array(
        'portal' => 'portal',
        'portal/*' => 'portal/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'menu-menu-portal' => array(
          'module' => 'menu',
          'delta' => 'menu-portal',
          'region' => 'menu_bar',
          'weight' => '-12',
        ),
      ),
    ),
    'menu' => 'portal',
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('HD Music');
  $export['music_selective'] = $context;

  return $export;
}
